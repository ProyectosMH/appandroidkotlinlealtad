package java.app.lealtad.videogames.login.viewmodel

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.ViewModel

import java.app.lealtad.videogames.login.interfaces.EventsActivityCallback
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.login.repository.LoginRepository
import java.app.lealtad.videogames.login.view.MainActivity
import java.app.lealtad.videogames.login.view.RecoveryPassActivity
import java.app.lealtad.videogames.users.view.CreateUserActivity

class LoginViewModel(private val loginActivityCallback: EventsActivityCallback, private val context: Context) : ViewModel() {
    private val user: Users
    var email: String = ""
    var phone: String = ""
    var newPass: String = ""
    var confirmNewPass: String = ""

    init {
        this.user = Users()
    }

    fun OnClick(view: View) {
        if (user.isCorrect) {
            loginActivityCallback.ProgresBarStar()
            val loginRepo = LoginRepository(loginActivityCallback)
            loginRepo.getTokenValidate(user, context)
        } else {
            loginActivityCallback.OnError("Favor ingrese su usuario y contraseña")
        }
    }

    val getUserText: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                user.username = editable.toString()
            }
        }

    val getPassText: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                user.pass = editable.toString()
            }
        }

    val getEmailText: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                email = editable.toString()
            }
        }

    val getPhoneText: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                phone = editable.toString()
            }
        }

    val getNewPassText: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                newPass = editable.toString()
            }
        }

    val getConfNewPassText: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                confirmNewPass = editable.toString()
            }
        }

    fun getNewPass(view: View){
        val intent= Intent(context, RecoveryPassActivity::class.java)
        context?.startActivity(intent)
    }

    fun changePass(view: View){
        if(!user.username.equals("")){
            if(!email.equals("")){
                if(!phone.equals("")){
                    if(!newPass.equals("")){
                        if(!confirmNewPass.equals("")){
                            if(newPass == confirmNewPass){
                                loginActivityCallback.ProgresBarStar()
                                val loginRepo = LoginRepository(loginActivityCallback)
                                loginRepo.recoveryPass(user.username!!, phone, email, newPass)
                            }else{
                                loginActivityCallback.OnError("Las contraseñas no coinciden")
                            }
                        }else{
                            loginActivityCallback.OnError("Campo confirma nueva contraseña es requerido")
                        }
                    }else{
                        loginActivityCallback.OnError("Campo nueva contraseña es requerido")
                    }
                }else{
                    loginActivityCallback.OnError("Campo telefono es requerido")
                }
            }else{
                loginActivityCallback.OnError("Campo email es requerido")
            }
        }else{
            loginActivityCallback.OnError("Campo usuario es requerido")
        }
    }

    fun goHome(view: View){
        val loginRepo = LoginRepository(loginActivityCallback)
        loginRepo.goHome(context)
    }

    fun Registrarse(view: View){
        val intent= Intent(context, CreateUserActivity::class.java)
        context?.startActivity(intent)
    }
}
