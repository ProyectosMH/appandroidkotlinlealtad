package java.app.lealtad.videogames.points.interfaces;

import java.app.lealtad.videogames.login.model.Users;

public interface PointsActivityCallback {
    void OnSuccess(String message);
    void OnError(String message);
    void ProgresBarStar();
    void LoadUserByName(Users user);
    void deletePoint(String idpoint);
}

