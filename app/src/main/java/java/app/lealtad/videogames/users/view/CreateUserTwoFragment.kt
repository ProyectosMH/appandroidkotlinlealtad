package java.app.lealtad.videogames.users.view

import android.app.Activity
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.users.interfaces.UsersActivityCallback
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner
import android.app.DatePickerDialog
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_create_user_two.*
import java.app.lealtad.videogames.login.model.Users
import java.text.SimpleDateFormat
import java.util.*
import java.lang.Exception


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CreateUserTwoFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [CreateUserTwoFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CreateUserTwoFragment(user: Users, idRolUser: String) : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    lateinit var usersActivityCallback: UsersActivityCallback
    var SPINNERLIST = arrayOf("Administrador", "Empleado", "Normal")
    var rol: String = ""
    var idRol: Int = 0
    private val CERO = "0"
    private val BARRA = "/"
    val c = Calendar.getInstance()
    lateinit var etFechaCumple: EditText
    var user = user
    var idRolUser = idRolUser

    //Variables para obtener la fecha
    val mes = c.get(Calendar.MONTH)
    val dia = c.get(Calendar.DAY_OF_MONTH)
    val anio = c.get(Calendar.YEAR)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_create_user_two, container, false)
        val arrayAdapter = ArrayAdapter<String>(
            activity,
            android.R.layout.simple_dropdown_item_1line, SPINNERLIST
        )
        val materialDesignSpinner =
            view.findViewById(R.id.msRol) as MaterialBetterSpinner
        materialDesignSpinner.setAdapter<ArrayAdapter<String>>(arrayAdapter)

        materialDesignSpinner.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, position, l ->
                rol = adapterView.getItemAtPosition(position).toString()
                //Toast.makeText(activity, spinner_value, Toast.LENGTH_LONG).show()
            }

        if(idRolUser != "1"){
            materialDesignSpinner.visibility = View.GONE
            rol = "Normal"
        }

        val ibFechaCumple = view.findViewById<View>(R.id.ibFechaCumple) as ImageView
        etFechaCumple = view.findViewById<View>(R.id.etFechaCumple) as EditText

        ibFechaCumple.setOnClickListener(View.OnClickListener {
            obtenerFecha()
        })

        val btnGuardar = view.findViewById<View>(R.id.btnGuardar) as Button
        val btnRegresar: Button = view.findViewById<View>(R.id.btnRegresar) as Button

        btnGuardar.setOnClickListener {
            val etFechaCumple = view.findViewById<View>(R.id.etFechaCumple) as EditText
            val etUser = view.findViewById<View>(R.id.etUser) as EditText
            val etPass = view.findViewById<View>(R.id.etPass) as EditText
            val etConfirmPass = view.findViewById<View>(java.app.lealtad.videogames.R.id.etConfirmPass) as EditText
            val currentDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())

            if(rol == ""){
                activity?.let { it1 -> Toasty.error(it1,"Campo rol de usuario es requerido!", Toast.LENGTH_SHORT).show() }
            }  else if(etFechaCumple.text.toString() == ""){
                activity?.let { it1 -> Toasty.error(it1,"Campo fecha cumpleaños es requerido!", Toast.LENGTH_SHORT).show() }
            } else if(etFechaCumple.text.toString().length != 10){
                activity?.let { it1 -> Toasty.error(it1,"Campo fecha cumpleaños es incorrecta!", Toast.LENGTH_SHORT).show() }
            } else if(etUser.text.toString() == ""){
                activity?.let { it1 -> Toasty.error(it1,"Campo usuario es requerido!", Toast.LENGTH_SHORT).show() }
            } else if(etPass.text.toString() == ""){
                activity?.let { it1 -> Toasty.error(it1,"Campo contraseña es requerido!", Toast.LENGTH_SHORT).show() }
            } else if(etConfirmPass.text.toString() == ""){
                activity?.let { it1 -> Toasty.error(it1,"Campo confirmar contraseña es requerido!", Toast.LENGTH_SHORT).show() }
            } else if(etPass.text.toString() != etConfirmPass.text.toString()){
                activity?.let { it1 -> Toasty.error(it1,"Las contraseñas no coinciden!", Toast.LENGTH_SHORT).show() }
            } else {
                if(compararFechas(etFechaCumple.text.toString())){
                    if(rol == "Administrador"){
                        idRol = 1
                    }else if(rol == "Empleado"){
                        idRol = 2
                    }else{
                        idRol = 3
                    }

                    user.pass = etPass.text.toString()
                    user.username = etUser.text.toString()
                    user.dateadd = currentDate
                    user.dateborn = etFechaCumple.text.toString()
                    user.idrol = idRol
                    usersActivityCallback.ValidateFragmentTwo(user)
                } else{
                    usersActivityCallback.OnError("La fecha de nacimiento no puede ser mayor o igual a la fecha actual.")
                }
            }
        }

        btnRegresar.setOnClickListener{
            usersActivityCallback.backOneFragment(user)
        }

        return view
    }

    fun compararFechas(fechaCumple: String): Boolean {
        try {
            val sdf =
                SimpleDateFormat("dd/MM/yyyy") // here set the pattern as you date in string was containing like date/month/year
            val cumple = sdf.parse(fechaCumple)
            val dateToday = Calendar.getInstance().time
            val dateHoy = sdf.format(dateToday)
            val dateTodayFinal = sdf.parse(dateHoy)
            val comparecumpleHoy = cumple.compareTo(dateTodayFinal)


            return !(comparecumpleHoy > 0 || comparecumpleHoy == 0)
        } catch (ex: Exception) {
            activity?.let { it1 -> Toasty.error(it1,ex.message.toString(), Toast.LENGTH_SHORT).show() }
            return false
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    private fun obtenerFecha() {
        val recogerFecha = DatePickerDialog(activity,
            DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val mesActual = month + 1
                val diaFormateado =
                    if (dayOfMonth < 10) CERO + dayOfMonth.toString() else dayOfMonth.toString()
                val mesFormateado =
                    if (mesActual < 10) CERO + mesActual.toString() else mesActual.toString()

                etFechaCumple.setText(diaFormateado + BARRA + mesFormateado + BARRA + year)
            }, anio, mes, dia
        )
        recogerFecha.show()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        try {
            usersActivityCallback = getActivity() as UsersActivityCallback
        } catch (e: ClassCastException) {
            throw ClassCastException("$activity must implement onButtonPressed")
        }

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }
}
