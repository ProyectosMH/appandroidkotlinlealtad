package java.app.lealtad.videogames.products.viewmodel

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.app.lealtad.videogames.home.model.Products
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.products.interfaces.ProductsActivityCallback
import java.app.lealtad.videogames.products.repository.ProductsRepository
import java.app.lealtad.videogames.products.view.ProductsActivity

class EditProductsViewModel : ViewModel {
    lateinit var productsActivityCallback: ProductsActivityCallback
    lateinit var context: Context
    var productsRepo: ProductsRepository

    var promocion: String = ""
    var puntos: String = ""
    var liveData = MutableLiveData<List<Products>>()
    var productName: String = ""
    var price: String = ""
    var quantity: String = ""
    var descripcion: String = ""
    var idProducto: String = ""

    constructor(productsActivityCallback: ProductsActivityCallback, context: Context) {
        this.productsActivityCallback = productsActivityCallback
        this.context = context
        productsRepo = ProductsRepository(productsActivityCallback, context)
    }

    fun goHome(view: View){
        val intent= Intent(context, HomeActivity::class.java)
        context?.startActivity(intent)
    }

    fun isPromocion(isPromo: String){
        promocion = isPromo
        if(promocion.equals("Si")){
            puntos = "0"
            productsActivityCallback.isPromotion(true)
        }else{
            productsActivityCallback.isPromotion(false)
        }
    }

    val getIdProd: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                idProducto = editable.toString()
            }
        }

    val getProductName: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                productName = editable.toString()
            }
        }

    val getPrice: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                price = editable.toString()
            }
        }

    val getPuntos: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                puntos = editable.toString()
            }
        }

    val getQuantity: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                quantity = editable.toString()
            }
        }

    val getDescripcion: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                descripcion = editable.toString()
            }
        }

    fun editProduct(view: View) {
        if(productName.equals("")){
            productsActivityCallback.OnError("Campo nombre de producto es requerido")
        }else if(price.equals("")){
            productsActivityCallback.OnError("Campo precio es requerido")
        }else if(puntos.equals("")){
            productsActivityCallback.OnError("Campo puntos es requerido")
        }else if(quantity.equals("")){
            productsActivityCallback.OnError("Campo existencia es requerido")
        }else if(descripcion.equals("")) {
            productsActivityCallback.OnError("Campo descripción es requerido")
        }else{
            productsActivityCallback.ProgresBarStar()
            val product = Products(idProducto, productName, price, puntos, quantity, descripcion, "", promocion)
            productsRepo.editProduct(product)
        }
    }

    fun backProductList(view: View) {
        val intent= Intent(context, ProductsActivity::class.java)
        context?.startActivity(intent)
    }

    fun seteoCampos(description: String, points: String, existencia: String, precio: String, nombre: String, idProd: String, promo: String) {
        promocion = promo
        puntos = points
        productName = nombre
        price = precio
        quantity = existencia
        descripcion = description
        idProducto = idProd
    }
}