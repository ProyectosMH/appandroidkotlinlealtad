package java.app.lealtad.videogames.points.interfaces

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.app.lealtad.videogames.login.model.ResponseObject
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.points.model.Points
import java.app.lealtad.videogames.points.model.PointsResponse

interface PointsServices {
    @POST("points/getPointList")
    fun getPointsList(@Header("idUser") idUser: String, @Header("idRol") idRol: String): Call<List<PointsResponse>>

    @POST("points/savePoints")
    fun addPoints(@Body points: RequestBody): Call<ResponseObject>

    @POST("users/findUserByName")
    fun findUserByName(@Header("username") username: String): Call<Users>

    @POST("points/deletePointsById")
    fun deletePoint(@Header("idpoint") idpoint: Int, @Header("iduseradd") iduseradd: Int): Call<ResponseObject>
}