package java.app.lealtad.videogames.home.viewmodel

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.app.lealtad.videogames.home.interfaces.HomeActivityCallback
import java.app.lealtad.videogames.home.model.Products
import java.app.lealtad.videogames.home.repository.HomeRepository
import java.app.lealtad.videogames.points.model.PointsResponse
import java.app.lealtad.videogames.users.view.CreateUserActivity

class HomeViewModel : ViewModel {

    lateinit var homeActivityCallback: HomeActivityCallback
    lateinit var context: Context
    var homeRepo: HomeRepository
    var puntos: String = "0"

    @SerializedName("records")
    @Expose
    private var items: ArrayList<Products>? = null
    var nameProducto: String = ""

    var liveData = MutableLiveData<List<Products>>()

    val getNameProduct: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                nameProducto = editable.toString()
            }
        }

    fun findProductbyName(view: View) {
        homeActivityCallback.ProgresBarStar()
        homeRepo.findProductbyName(nameProducto, puntos)
    }

    fun findAllProducts(view: View) {
        homeActivityCallback.ProgresBarStar()
        homeActivityCallback.IsLoadList("No")
        //homeRepo.findAllProducts(puntos, "No")
    }

    fun findPromociones(view: View) {
        homeActivityCallback.ProgresBarStar()
        homeActivityCallback.IsLoadList("Si")
        //homeRepo.findAllProducts(puntos, "Si")
    }

    fun getProducts(puntos: String, promo: String) {
        homeActivityCallback.ProgresBarStar()
        homeRepo.findAllProducts(puntos, promo)
    }

    fun getDataProducts(points: String): MutableLiveData<List<Products>>{
        puntos = points
        liveData = homeRepo.getProductsMutableLiveData(points)
        return liveData
    }

    constructor(homeActivityCallback: HomeActivityCallback, context: Context) {
        this.homeActivityCallback = homeActivityCallback
        this.context = context
        homeRepo = HomeRepository(homeActivityCallback)
    }

    fun getDataPoints(): MutableLiveData<PointsResponse>{
        homeActivityCallback.ProgresBarStar()
        return homeRepo.LoadPoints(context)
    }
}