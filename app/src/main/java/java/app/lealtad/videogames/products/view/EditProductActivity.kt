package java.app.lealtad.videogames.products.view

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.databinding.ActivityEditProductBinding
import java.app.lealtad.videogames.home.model.Products
import java.app.lealtad.videogames.products.interfaces.ProductsActivityCallback
import java.app.lealtad.videogames.products.viewmodel.EditProductsViewModel
import java.app.lealtad.videogames.products.viewmodel.EditProductsViewModelFactory
import java.app.lealtad.videogames.util.BaseActivity

class EditProductActivity : BaseActivity(), ProductsActivityCallback {

    var editProductsViewModel: EditProductsViewModel? = null
    var MyVersion: Int = Build.VERSION.SDK_INT
    var SPINNERLIST = arrayOf("Si", "No")
    var materialDesignSpinner: MaterialBetterSpinner? = null
    var tilPuntos: TextInputLayout? = null
    var isPromo: String = ""
    var etPrice: TextInputEditText? = null
    var etPuntos: TextInputEditText? = null
    var etExistencia: TextInputEditText? = null
    var etDescripcion: TextInputEditText? = null
    var etNombre: TextInputEditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_edit_product)
        title = "Editar Productos"

        val productMainBinding = DataBindingUtil.setContentView<ActivityEditProductBinding>(this, R.layout.activity_edit_product)
        productMainBinding.product = ViewModelProviders.of(this, EditProductsViewModelFactory(this, this))
            .get(EditProductsViewModel::class.java)

        editProductsViewModel = ViewModelProviders.of(this, EditProductsViewModelFactory(this, this)).get(EditProductsViewModel::class.java)

        initializeView()

        materialDesignSpinner!!.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, position, l ->
                isPromo = adapterView.getItemAtPosition(position).toString()
                editProductsViewModel!!.isPromocion(isPromo)
            }
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, SPINNERLIST)
        materialDesignSpinner = findViewById(R.id.msPromo) as MaterialBetterSpinner
        materialDesignSpinner!!.setAdapter<ArrayAdapter<String>>(arrayAdapter)
        tilPuntos = findViewById<TextInputLayout>(R.id.tilPuntos)
        etPrice = findViewById<TextInputEditText>(R.id.etPrice)
        etPuntos = findViewById<TextInputEditText>(R.id.etPuntos)
        etExistencia = findViewById<TextInputEditText>(R.id.etExistencia)
        etDescripcion = findViewById<TextInputEditText>(R.id.etDescripcion)
        etNombre = findViewById<TextInputEditText>(R.id.etNombre)

        val intent = intent
        val idProd = intent.getStringExtra("IDPRODUCTO")
        val productName = intent.getStringExtra("PRODUCTNAME")
        val description = intent.getStringExtra("DESCRIPTION")
        val price = intent.getStringExtra("PRICE")
        val points = intent.getStringExtra("POINTS")
        val promo = intent.getStringExtra("PROMOTION")
        val existencia = intent.getStringExtra("EXISTENCIA")

        etDescripcion!!.setText(description)
        etPuntos!!.setText(points)
        if(promo.equals("Si")){
            tilPuntos!!.visibility = View.GONE
        }
        etExistencia!!.setText(existencia)
        etPrice!!.setText(price)
        etNombre!!.setText(productName)
        materialDesignSpinner!!.setText(promo)

        editProductsViewModel!!.seteoCampos(description, points, existencia, price, productName, idProd, promo)
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnSuccess(message: String) {
        ProgresbarFinish()
        if(!message.equals("")){
            Toasty.success(this, message, Toast.LENGTH_SHORT, true).show()
            val intent= Intent(applicationContext, ProductsActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun OnSuccessProducts(productsList: List<Products>) {
        ProgresbarFinish()
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }

    override fun deleteProduct(idproduct: String) {
    }

    override fun isPromotion(promotion: Boolean) {
        if(promotion){
            tilPuntos!!.visibility = View.GONE
        }else{
            tilPuntos!!.visibility = View.VISIBLE
        }
    }
    //endregion View
}
