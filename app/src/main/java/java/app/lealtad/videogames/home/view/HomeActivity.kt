package java.app.lealtad.videogames.home.view

import android.Manifest
import android.app.Dialog
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.home.interfaces.HomeActivityCallback
import java.app.lealtad.videogames.home.viewmodel.HomeViewModel
import java.app.lealtad.videogames.home.viewmodel.HomeViewModelFactory
import java.app.lealtad.videogames.util.BaseActivity
import androidx.recyclerview.widget.RecyclerView
import java.app.lealtad.videogames.home.adapter.HomeAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import java.app.lealtad.videogames.databinding.ActivityHomeBinding
import java.app.lealtad.videogames.points.model.PointsResponse
import java.app.lealtad.videogames.home.model.Products
import java.text.SimpleDateFormat
import java.util.*

class HomeActivity : BaseActivity(), HomeActivityCallback {

    lateinit var tvPoints: TextView
    lateinit var llAllProducts: LinearLayout
    lateinit var llPromoProducts: LinearLayout
    var homeViewModel: HomeViewModel? = null
    var homeAdapter: HomeAdapter? = null
    var recyclerView: RecyclerView? = null
    var idUser: String = ""
    var nombreUser: String = ""
    var puntos: String = ""
    var btnAceptar: Button? = null
    var promoList: List<Products>? = null
    var productList: List<Products>? = null
    private val REQUEST_WRITE_PERMISION = 112

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_home)

        val homeMainBinding = DataBindingUtil.setContentView<ActivityHomeBinding>(this, R.layout.activity_home)
        homeMainBinding.products = ViewModelProviders.of(this, HomeViewModelFactory(this, this))
            .get(HomeViewModel::class.java)

        homeViewModel = ViewModelProviders.of(this, HomeViewModelFactory(this, this)).get(HomeViewModel::class.java)

        initializeView()

        hasStoragePermission(REQUEST_WRITE_PERMISION)

        recyclerView = findViewById<RecyclerView>(R.id.recyclerProducts)

        homeViewModel!!.getDataPoints().observe(this,
            Observer<PointsResponse> { points ->
                this.puntos = points.totals.toString()
                tvPoints.text = points.totals.toString()

                homeViewModel!!.getDataProducts(points.totals.toString()).observe(this,
                    Observer<List<Products>> { arrayList ->
                        if(arrayList != null){
                            this.productList = arrayList
                            homeAdapter = HomeAdapter(this@HomeActivity, arrayList, idUser, nombreUser)

                            recyclerView!!.layoutManager = LinearLayoutManager(this@HomeActivity)
                            recyclerView!!.adapter = homeAdapter
                        }
                    })
            })
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        idUser = getValueSession(IDUSER_S, applicationContext, SESSION_PREFERENCES)
        nombreUser = getValueSession(USER_S, applicationContext, SESSION_PREFERENCES)
        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        llAllProducts = findViewById<LinearLayout>(R.id.llAllProducts)
        llPromoProducts = findViewById<LinearLayout>(R.id.llPromoProducts)
        tvPoints = findViewById<TextView>(R.id.tvPoints)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        var dateBorn: String = getValueSession(DATEBORN_S, applicationContext, CUMPLE_PREFERENCES)
        val currentDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        if(dateBorn != "default value"){
            if(dateBorn.split("/")[1] == currentDate.split("/")[1]){
                openDialogBorn()
            }
        }
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun IsLoadList(promo: String) {
        if(promo.equals("Si")){
            if(this.promoList != null){
                OnSuccessProducts(promoList!!, promo)
            }else{
                homeViewModel!!.getProducts(this.puntos, promo)
            }
        }else {
            if(this.productList != null){
                OnSuccessProducts(productList!!, promo)
            }else{
                homeViewModel!!.getProducts(this.puntos, promo)
            }
        }
    }

    override fun OnSuccess(promo: String) {
        ProgresbarFinish()
        if(promo == "No"){
            val colorValue = ContextCompat.getColor(applicationContext, R.color.black_effective)
            llAllProducts.setBackgroundColor(colorValue)
            val colorValue2 = ContextCompat.getColor(applicationContext, R.color.semi_gray)
            llPromoProducts.setBackgroundColor(colorValue2)
        }else{
            val colorValue = ContextCompat.getColor(applicationContext, R.color.black_effective)
            llPromoProducts.setBackgroundColor(colorValue)
            val colorValue2 = ContextCompat.getColor(applicationContext, R.color.semi_gray)
            llAllProducts.setBackgroundColor(colorValue2)
        }
    }

    override fun OnSuccessProducts(productsList: List<Products>, promo: String) {
        if(promo.equals("Si")){
            this.promoList = productsList
        }else {
            this.productList = productsList
        }

        ProgresbarFinish()
        if(productsList != null){
            homeAdapter = HomeAdapter(this@HomeActivity, productsList, idUser, nombreUser)

            recyclerView!!.layoutManager = LinearLayoutManager(this@HomeActivity)
            recyclerView!!.adapter = homeAdapter
            OnSuccess(promo)
        }
    }

    override fun OnSuccessPoints(points: String?) {
        ProgresbarFinish()
        tvPoints.text = points
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }

    fun openDialogBorn(): Dialog {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.born_dialog)
        dialog.show()
        dialog.setCanceledOnTouchOutside(false)

        btnAceptar = dialog.findViewById(R.id.btnAceptar) as Button

        btnAceptar!!.setOnClickListener {
            deleteCumple()
            dialog.dismiss()
        }

        return dialog
    }

    private fun hasStoragePermission(requestCode: Int): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), requestCode)
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            /*if (requestCode == FILE_ATTACHMENT){

            }*/
        }
    }
    //endregion View
}
