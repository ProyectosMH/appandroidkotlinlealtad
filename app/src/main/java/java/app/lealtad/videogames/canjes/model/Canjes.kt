package java.app.lealtad.videogames.canjes.model

import androidx.databinding.BaseObservable
import android.graphics.Bitmap
import android.widget.ImageView
import androidx.databinding.BindingAdapter

public class Canjes : BaseObservable {

    lateinit var idcanje: String
    lateinit var iduser: String
    lateinit var idproduct: String
    lateinit var nameproduct: String
    lateinit var pointsprodct: String
    lateinit var pointuser: String
    lateinit var totalpointuser: String
    lateinit var iduseradd: String
    lateinit var dateadd: String
    lateinit var idusermod: String
    lateinit var datemod: String
    lateinit var username: String
    var useradd: String = ""
    var showDeleteButon: Boolean = false

    constructor() {}

    constructor(idcanje: String, iduser: String, idproduct: String, nameproduct: String, pointsprodct: String, pointuser: String, totalpointuser: String, iduseradd: String, dateadd: String, idusermod: String, datemod: String, username: String, useradd: String) {
        this.idcanje = idcanje
        this.iduser = iduser
        this.idproduct = idproduct
        this.nameproduct = nameproduct
        this.pointsprodct = pointsprodct
        this.pointuser = pointuser
        this.totalpointuser = totalpointuser
        this.iduseradd = iduseradd
        this.dateadd = dateadd
        this.idusermod = idusermod
        this.datemod = datemod
        this.username = username
        this.useradd = useradd
    }
}