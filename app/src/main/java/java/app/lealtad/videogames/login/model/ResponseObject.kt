package java.app.lealtad.videogames.login.model

import androidx.databinding.BaseObservable

class ResponseObject : BaseObservable {
    lateinit var message: String
    var success: Boolean = false

    constructor() {}

    constructor(message: String, success: Boolean) {
        this.message = message
        this.success = success
    }
}