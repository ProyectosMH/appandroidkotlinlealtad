package java.app.lealtad.videogames.points.viewmodel

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.points.model.PointsResponse
import java.app.lealtad.videogames.points.interfaces.PointsActivityCallback
import java.app.lealtad.videogames.points.model.Points
import java.app.lealtad.videogames.points.repository.PointsRepository
import java.app.lealtad.videogames.points.view.AddPointsActivity

class PointsViewModel: ViewModel {

    var pointsActivityCallback: PointsActivityCallback
    var context: Context

    @SerializedName("records")
    @Expose
    private var items: ArrayList<PointsResponse>? = null

    var liveData = MutableLiveData<List<PointsResponse>>()
    var fullName: String = ""
    var userName: String = ""
    var showFloting: Boolean = false
    var pointRepo: PointsRepository

    fun goHome(view: View){
        val intent= Intent(context, HomeActivity::class.java)
        context?.startActivity(intent)
    }

    fun addPoints(view: View) {
        val intent= Intent(context, AddPointsActivity::class.java)
        context?.startActivity(intent)
    }

    val getNameUser: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                userName = editable.toString()
            }
        }

    fun findProductbyName(view: View) {
        pointsActivityCallback.ProgresBarStar()
        pointRepo.findProductbyName(userName)
    }

    fun getDataPoints(): MutableLiveData<List<PointsResponse>> {
        pointsActivityCallback.ProgresBarStar()
        liveData = pointRepo.getPointsMutableLiveData()
        return liveData
    }

    fun deletePoint(idpoint: String){
        pointsActivityCallback.ProgresBarStar()
        pointRepo.deletePoint(idpoint)
    }

    constructor(pointsActivityCallback: PointsActivityCallback, context: Context) {
        this.pointsActivityCallback = pointsActivityCallback
        this.context = context
        pointRepo = PointsRepository(pointsActivityCallback, context)
        this.showFloting = pointRepo.flotingShow()
    }
}