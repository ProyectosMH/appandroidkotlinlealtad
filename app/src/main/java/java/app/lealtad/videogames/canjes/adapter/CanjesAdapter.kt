package java.app.lealtad.videogames.canjes.adapter

import android.app.Dialog
import androidx.recyclerview.widget.RecyclerView
import java.app.lealtad.videogames.R
import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.annotation.NonNull
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.canjes.interfaces.CanjesActivityCallback
import java.app.lealtad.videogames.canjes.model.Canjes
import java.app.lealtad.videogames.canjes.view.CanjesDetailsActivity
import java.app.lealtad.videogames.databinding.CanjesBinding
import java.app.lealtad.videogames.databinding.PointsBinding
import java.app.lealtad.videogames.points.model.PointsResponse

class CanjesAdapter(private val context: Context, private val arrayList: List<Canjes>, private val canjesActivityCallback: CanjesActivityCallback) : RecyclerView.Adapter<CanjesAdapter.CustomView>() {

    private var layoutInflater: LayoutInflater? = null
    private var btnSi: Button? = null
    private var btnNo: Button? = null

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): CustomView {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }

        val cardBinding = layoutInflater?.let {
            DataBindingUtil.inflate<CanjesBinding>(
                layoutInflater!!,
                R.layout.canjes_list,
                parent,
                false
            )
        }

        return cardBinding?.let { CustomView(it) }!!
    }

    override fun onBindViewHolder(@NonNull holder: CustomView, position: Int) {
        val canjesViewModel = arrayList[position]
        holder.bind(canjesViewModel)

        holder.canjesBinding.btnDetalle.setOnClickListener {
            val intent = Intent(context, CanjesDetailsActivity::class.java)
            intent.putExtra("USERNAME", holder.canjesBinding.canjes!!.username)
            intent.putExtra("PRODUCTO", holder.canjesBinding.canjes!!.nameproduct)
            intent.putExtra("POINTS", holder.canjesBinding.canjes!!.pointsprodct)
            intent.putExtra("POINTSUSER", holder.canjesBinding.canjes!!.pointuser)
            intent.putExtra("TOTALS", holder.canjesBinding.canjes!!.totalpointuser)
            intent.putExtra("USERADD", holder.canjesBinding.canjes!!.useradd)
            intent.putExtra("DATEADD", holder.canjesBinding.canjes!!.dateadd)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context?.startActivity(intent)
        }

        holder.canjesBinding.ivDeletepoint.setOnClickListener {
            openDialogDelete(holder.canjesBinding.canjes!!.idcanje)
        }
    }

    fun openDialogDelete(idcanje: String): Dialog {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.delete_dialog)
        dialog.show()
        dialog.setCanceledOnTouchOutside(false)

        btnNo = dialog.findViewById(R.id.btnNo) as Button
        btnSi = dialog.findViewById(R.id.btnSi) as Button

        btnNo!!.setOnClickListener { dialog.dismiss() }

        btnSi!!.setOnClickListener {
            dialog.dismiss()
            canjesActivityCallback.deleteCanje(idcanje)
        }

        return dialog
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class CustomView(var canjesBinding: CanjesBinding) : RecyclerView.ViewHolder(canjesBinding.root) {

        fun bind(canjes: Canjes) {
            this.canjesBinding.canjes = canjes
            canjesBinding.executePendingBindings()
        }
    }
}
