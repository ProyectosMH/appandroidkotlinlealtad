package java.app.lealtad.videogames.home.adapter

import androidx.recyclerview.widget.RecyclerView
import java.app.lealtad.videogames.R
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import java.app.lealtad.videogames.databinding.CardBinding
import java.app.lealtad.videogames.home.model.Products
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.widget.Button
import net.glxn.qrgen.android.QRCode
import android.widget.ImageView
import android.util.Base64
import android.util.Log
import java.app.lealtad.videogames.products.view.ProductDetailsActivity
import java.io.*

class HomeAdapter(private val context: Context, private val arrayList: List<Products>, private val idUser: String, private val userName: String) : RecyclerView.Adapter<HomeAdapter.CustomView>() {

    private var layoutInflater: LayoutInflater? = null
    private var btnRegresar: Button? = null
    private var ivQR: ImageView? = null
    private var btnSi: Button? = null
    private var btnNo: Button? = null
    var filePath: String = ""

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): CustomView {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }

        val cardBinding = layoutInflater?.let {
            DataBindingUtil.inflate<CardBinding>(layoutInflater!!, R.layout.products_list, parent, false)
        }

        return cardBinding?.let { CustomView(it) }!!
    }

    override fun onBindViewHolder(@NonNull holder: CustomView, position: Int) {
        val homeViewModel = arrayList[position]
        holder.bind(homeViewModel)

        holder.cardBinding.cvDetalle.setOnClickListener{
            val intent = Intent(context, ProductDetailsActivity::class.java)
            intent.putExtra("PRODUCTNAME", holder.cardBinding.products!!.nameproduct)
            intent.putExtra("DESCRIPTION", holder.cardBinding.products!!.description)
            //var peso: Double = calcBase64SizeInKBytes(holder.cardBinding.products!!.image)
            var image: Bitmap = decodeImage(holder.cardBinding.products!!.image)
            saveImage(image, "imageVideoGa.png")
            intent.putExtra("IMAGEBASE", filePath)
            intent.putExtra("PRICE", holder.cardBinding.products!!.price)
            intent.putExtra("POINTS", holder.cardBinding.products!!.points)
            intent.putExtra("PROMOTION", holder.cardBinding.products!!.promocion)
            //intent.putExtra("productList", arrayList as Serializable)
            intent.putExtra("ACTIVITY", "HomeActivity")
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context?.startActivity(intent)
        }

        holder.cardBinding.btnCanjear.setOnClickListener {
            var texto: String = idUser + "~" + userName + "~" + holder.cardBinding.products!!.idproduct + "~" + holder.cardBinding.products!!.nameproduct + "~" + holder.cardBinding.products!!.points + "~" + holder.cardBinding.products!!.totalPuntosUser
            openDialogFinger(texto)
        }
    }

    fun saveImage(imageData: Bitmap, filename: String):Boolean {
        // get path to external storage (SD card)

        var sdIconStorageDir:File? = null

        sdIconStorageDir = File(
            Environment.getExternalStorageDirectory()
                .absolutePath + "/myAppDir/"
        )
        // create storage directories, if they don't exist
        if (!sdIconStorageDir!!.exists())
        {
            sdIconStorageDir!!.mkdirs()
        }
        try
        {
            filePath = sdIconStorageDir!!.toString() + File.separator + filename
            val fileOutputStream = FileOutputStream(filePath)
            val bos = BufferedOutputStream(fileOutputStream)
            imageData.compress(Bitmap.CompressFormat.PNG, 100, bos)
            bos.flush()
            bos.close()
        }
        catch (e: FileNotFoundException) {
            Log.w("TAG", "Error saving image file: " + e.message)
            return false
        }
        catch (e:IOException) {
            Log.w("TAG", "Error saving image file: " + e.message)
            return false
        }

        return true
    }

    fun decodeImage(input: String): Bitmap {
        val decodedBytes = Base64.decode(input, 0)
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
    }

    fun openDialogFinger(texto: String): Dialog {
        val textoEncr = encriptar(texto)
        val bitmap = QRCode.from(textoEncr).withSize(550, 550).bitmap()
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.canjear_qr)
        dialog.show()
        dialog.setCanceledOnTouchOutside(false)

        ivQR = dialog.findViewById(R.id.ivQR) as ImageView
        ivQR!!.setImageBitmap(bitmap)

        btnRegresar = dialog.findViewById(R.id.btnRegresar) as Button

        btnRegresar!!.setOnClickListener { dialog.dismiss() }

        return dialog
    }

    fun encriptar(value: String): String {
        try {
            val encrpt = value.toByteArray(charset("UTF-8"))
            return Base64.encodeToString(encrpt, Base64.NO_WRAP)
        } catch (e: Exception) {
            return "error"
        }

    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class CustomView(var cardBinding: CardBinding) : RecyclerView.ViewHolder(cardBinding.root) {

        fun bind(products: Products) {
            this.cardBinding.products = products
            cardBinding.executePendingBindings()
        }
    }
}
