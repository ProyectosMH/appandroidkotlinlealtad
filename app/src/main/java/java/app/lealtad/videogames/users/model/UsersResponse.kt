package java.app.lealtad.videogames.users.model

import androidx.databinding.BaseObservable

public class UsersResponse : BaseObservable {

    lateinit var iduser: String
    lateinit var firstname: String
    lateinit var secondname: String
    lateinit var surname: String
    lateinit var secondsurname: String
    lateinit var email: String
    lateinit var phone: String
    lateinit var username: String
    lateinit var dateborn: String
    lateinit var useradd: String
    lateinit var dateadd: String
    lateinit var rol: String
    lateinit var  rolname: String

    constructor() {}

    constructor(iduser: String, firstname: String, secondname: String, surname: String, secondsurname: String, email: String, phone: String, username: String, dateborn: String, useradd: String, dateadd: String, rol: String, rolname: String) {
        this.iduser = iduser
        this.firstname = firstname
        this.secondname = secondname
        this.surname = surname
        this.secondsurname = secondsurname
        this.email = email
        this.phone = phone
        this.username = username
        this.dateborn = dateborn
        this.useradd = useradd
        this.dateadd = dateadd
        this.rol = rol
        this.rolname = rolname
    }
}
