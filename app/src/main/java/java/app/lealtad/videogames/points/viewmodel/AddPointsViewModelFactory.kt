package java.app.lealtad.videogames.points.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.app.lealtad.videogames.points.interfaces.PointsActivityCallback

class AddPointsViewModelFactory (private val pointsActivityCallback: PointsActivityCallback, private  val context: Context) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AddPointsViewModel(pointsActivityCallback, context) as T
    }
}
