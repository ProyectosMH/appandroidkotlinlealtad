package java.app.lealtad.videogames.points.model

import androidx.databinding.BaseObservable

public class PointsResponse : BaseObservable {

    lateinit var idpoint: String
    lateinit var username: String
    var points: String = "0"
    var totals: String = "0"
    lateinit var codefactura: String
    lateinit var description: String
    lateinit var useradd: String
    lateinit var dateadd: String
    var showDeleteButon: Boolean = false

    constructor() {}

    constructor(idpoint: String, username: String, points: String, totals: String, codefactura: String, description: String, useradd: String, dateadd: String) {
        this.idpoint = idpoint
        this.username = username
        this.points = points
        this.totals = totals
        this.codefactura = codefactura
        this.description = description
        this.useradd = useradd
        this.dateadd = dateadd
    }
}
