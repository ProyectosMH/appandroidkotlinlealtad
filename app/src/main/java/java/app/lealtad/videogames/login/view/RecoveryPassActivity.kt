package java.app.lealtad.videogames.login.view

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.databinding.ActivityRecoveryPassBinding
import java.app.lealtad.videogames.login.interfaces.EventsActivityCallback
import java.app.lealtad.videogames.login.viewmodel.LoginViewModel
import java.app.lealtad.videogames.login.viewmodel.LoginViewModelFactory
import java.app.lealtad.videogames.util.BaseActivity

class RecoveryPassActivity : BaseActivity(), EventsActivityCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_recovery_pass)
        title = "Recuperar contraseña"

        val activityRecoveryPassBinding = DataBindingUtil.setContentView<ActivityRecoveryPassBinding>(this, R.layout.activity_recovery_pass)

        activityRecoveryPassBinding.pass = ViewModelProviders.of(this, LoginViewModelFactory(this, this))
            .get(LoginViewModel::class.java)

        initializeView()
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnSuccess() {
        ProgresbarFinish()
        Toasty.success(this, "Contraseña actualizada exitosamente", Toast.LENGTH_SHORT, true).show()
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }
    //endregion View
}
