package java.app.lealtad.videogames.login.interfaces;

public interface EventsActivityCallback {
    void OnSuccess();
    void OnError(String message);
    void ProgresBarStar();
}
