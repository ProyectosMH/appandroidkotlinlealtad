package java.app.lealtad.videogames.canjes.viewmodel

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.app.lealtad.videogames.canjes.interfaces.CanjesActivityCallback
import java.app.lealtad.videogames.canjes.model.Canjes
import java.app.lealtad.videogames.canjes.repository.CanjesRepository
import java.app.lealtad.videogames.canjes.view.AddCanjeActivity
import java.app.lealtad.videogames.home.view.HomeActivity

class CanjesViewModel: ViewModel {

    lateinit var canjesActivityCallback: CanjesActivityCallback
    lateinit var context: Context
    var canjesRepo: CanjesRepository
    var liveData = MutableLiveData<List<Canjes>>()
    var showFloting: Boolean = false

    fun goHome(view: View){
        val intent= Intent(context, HomeActivity::class.java)
        context?.startActivity(intent)
    }

    fun addCanje(view: View) {
        val intent= Intent(context, AddCanjeActivity::class.java)
        context?.startActivity(intent)
    }

    fun getDataPoints(): MutableLiveData<List<Canjes>> {
        canjesActivityCallback.ProgresBarStar()
        liveData = canjesRepo.getCanjesMutableLiveData(context)
        return liveData
    }

    fun deleteCanje(idcanje: String){
        canjesActivityCallback.ProgresBarStar()
        canjesRepo.deleteCanje(idcanje, context)
    }

    constructor(canjesActivityCallback: CanjesActivityCallback, context: Context) {
        this.canjesActivityCallback = canjesActivityCallback
        this.context = context
        canjesRepo = CanjesRepository(canjesActivityCallback)
        this.showFloting = canjesRepo.flotingShow(context)
    }
}