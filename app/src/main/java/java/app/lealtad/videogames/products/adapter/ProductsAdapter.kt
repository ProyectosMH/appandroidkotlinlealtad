package java.app.lealtad.videogames.home.adapter

import androidx.recyclerview.widget.RecyclerView
import java.app.lealtad.videogames.R
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import java.app.lealtad.videogames.databinding.CardBinding
import java.app.lealtad.videogames.home.model.Products
import android.app.Dialog
import android.content.Intent
import android.widget.Button
import net.glxn.qrgen.android.QRCode
import android.widget.ImageView
import android.util.Base64
import java.app.lealtad.videogames.products.interfaces.ProductsActivityCallback
import java.app.lealtad.videogames.products.view.EditProductActivity
import java.app.lealtad.videogames.products.view.ProductDetailsActivity

class ProductsAdapter(private val context: Context, private val arrayList: List<Products>, private val productsActivityCallback: ProductsActivityCallback,  private val idUser: String, private val userName: String) : RecyclerView.Adapter<ProductsAdapter.CustomView>() {

    private var layoutInflater: LayoutInflater? = null
    private var btnRegresar: Button? = null
    private var ivQR: ImageView? = null
    private var btnSi: Button? = null
    private var btnNo: Button? = null

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): CustomView {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }

        val cardBinding = layoutInflater?.let {
            DataBindingUtil.inflate<CardBinding>(layoutInflater!!, R.layout.products_list, parent, false)
        }

        return cardBinding?.let { CustomView(it) }!!
    }

    override fun onBindViewHolder(@NonNull holder: CustomView, position: Int) {
        val homeViewModel = arrayList[position]
        holder.bind(homeViewModel)

        holder.cardBinding.cvDetalle.setOnClickListener{
            val intent = Intent(context, ProductDetailsActivity::class.java)
            intent.putExtra("PRODUCTNAME", holder.cardBinding.products!!.nameproduct)
            intent.putExtra("DESCRIPTION", holder.cardBinding.products!!.description)
            intent.putExtra("IMAGEBASE", holder.cardBinding.products!!.image)
            intent.putExtra("PRICE", holder.cardBinding.products!!.price)
            intent.putExtra("POINTS", holder.cardBinding.products!!.points)
            intent.putExtra("PROMOTION", holder.cardBinding.products!!.promocion)
            intent.putExtra("ACTIVITY", "ProductsActivity")
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context?.startActivity(intent)
        }

        holder.cardBinding.ivDeleteProduct.setOnClickListener {
            openDialogDelete(holder.cardBinding.products!!.idproduct)
        }

        holder.cardBinding.ivEditProduct.setOnClickListener {
            val intent = Intent(context, EditProductActivity::class.java)
            intent.putExtra("IDPRODUCTO", holder.cardBinding.products!!.idproduct)
            intent.putExtra("PRODUCTNAME", holder.cardBinding.products!!.nameproduct)
            intent.putExtra("DESCRIPTION", holder.cardBinding.products!!.description)
            intent.putExtra("PRICE", holder.cardBinding.products!!.price)
            intent.putExtra("POINTS", holder.cardBinding.products!!.points)
            intent.putExtra("PROMOTION", holder.cardBinding.products!!.promocion)
            intent.putExtra("EXISTENCIA", holder.cardBinding.products!!.quantity)
            intent.putExtra("ACTIVITY", "ProductsActivity")
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context?.startActivity(intent)
        }
    }

    fun openDialogDelete(idproduct: String): Dialog {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.delete_dialog)
        dialog.show()
        dialog.setCanceledOnTouchOutside(false)

        btnNo = dialog.findViewById(R.id.btnNo) as Button
        btnSi = dialog.findViewById(R.id.btnSi) as Button

        btnNo!!.setOnClickListener { dialog.dismiss() }

        btnSi!!.setOnClickListener {
            dialog.dismiss()
            productsActivityCallback.deleteProduct(idproduct)
        }

        return dialog
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class CustomView(var cardBinding: CardBinding) : RecyclerView.ViewHolder(cardBinding.root) {

        fun bind(products: Products) {
            this.cardBinding.products = products
            cardBinding.executePendingBindings()
        }
    }
}
