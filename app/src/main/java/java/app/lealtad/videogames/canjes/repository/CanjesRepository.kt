package java.app.lealtad.videogames.canjes.repository

import android.content.Context
import android.content.Intent
import android.util.ArrayMap
import androidx.lifecycle.MutableLiveData
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.app.lealtad.videogames.canjes.interfaces.CanjesActivityCallback
import java.app.lealtad.videogames.canjes.interfaces.CanjesServices
import java.app.lealtad.videogames.canjes.model.Canjes
import java.app.lealtad.videogames.canjes.view.CanjesActivity
import java.app.lealtad.videogames.login.model.ResponseObject
import java.app.lealtad.videogames.util.ApiUtilToken
import java.app.lealtad.videogames.util.BaseActivity
import java.text.SimpleDateFormat
import java.util.*

class CanjesRepository(private val canjesActivityCallback: CanjesActivityCallback) : BaseActivity() {

    fun getCanjesMutableLiveData(context: Context): MutableLiveData<List<Canjes>> {
        var idUser = getValueSession(IDUSER_S, context, SESSION_PREFERENCES)
        var idRol = getValueSession(ROLUSER_S, context, SESSION_PREFERENCES)
        var liveData = MutableLiveData<List<Canjes>>()
        var service = ApiUtilToken.apiUtil?.create(CanjesServices::class.java)
        val call = service?.getCanjesList(idUser, idRol)

        call?.enqueue(object : Callback<List<Canjes>> {
            override fun onResponse(call: Call<List<Canjes>>, response: Response<List<Canjes>>) {
                if (response.code() == 200) {
                    liveData.value = response.body()!!
                    liveData.value!!.forEach {
                        if(idRol != "3"){
                            it.showDeleteButon =  true
                        }
                    }
                    canjesActivityCallback.OnSuccess("")
                }else{
                    canjesActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<List<Canjes>>, t: Throwable) {
                liveData.value = null
            }
        })

        return liveData
    }

    fun flotingShow(context: Context): Boolean{
        var isnotComunUSer: Boolean = false
        var idUser: String = getValueSession(ROLUSER_S, context, SESSION_PREFERENCES)
        if(idUser != "3"){
            isnotComunUSer = true
        }
        return isnotComunUSer
    }

    fun saveCanje(canje: Canjes, context: Context) {
        val jsonParams = ArrayMap<String, Any>()
        var userAddSession = getValueSession(IDUSER_S, context, SESSION_PREFERENCES)
        val currentDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        jsonParams.put("idcanje", 0)
        jsonParams.put("iduser", canje.iduser)
        jsonParams.put("idproduct", canje.idproduct)
        jsonParams.put("nameproduct", canje.nameproduct)
        jsonParams.put("pointsprodct", canje.pointsprodct)
        jsonParams.put("pointuser", canje.pointuser)
        jsonParams.put("totalpointuser", canje.totalpointuser)
        jsonParams.put("iduseradd", userAddSession)
        jsonParams.put("dateadd", currentDate)
        jsonParams.put("idusermod", 0)
        jsonParams.put("datemod", "")

        val body = RequestBody.create(
            okhttp3.MediaType.parse("application/json; charset=utf-8"),
            JSONObject(jsonParams).toString()
        )

        var service = ApiUtilToken.apiUtil?.create(CanjesServices::class.java)
        val call = service?.saveCanje(body)
        var responseObject: ResponseObject

        call?.enqueue(object : Callback<ResponseObject> {
            override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                if (response.code() == 200) {
                    responseObject = response.body()!!
                    if(responseObject.success){
                        responseObject = response.body()!!
                        canjesActivityCallback.OnSuccess("Canje Save")
                    }else {
                        canjesActivityCallback.OnError(responseObject.message)
                    }
                }else{
                    canjesActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                canjesActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }

    fun deleteCanje(idcanje: String, context: Context){
        var service = ApiUtilToken.apiUtil?.create(CanjesServices::class.java)
        val call = service?.deleteCanje(idcanje.toInt())
        var responseObject: ResponseObject

        call?.enqueue(object : Callback<ResponseObject> {
            override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                if (response.code() == 200) {
                    responseObject = response.body()!!
                    if(responseObject.success){
                        responseObject = response.body()!!
                        val intent = Intent(context, CanjesActivity::class.java)
                        context?.startActivity(intent)
                        finish()
                        canjesActivityCallback.OnSuccess(responseObject.message)
                    }else {
                        canjesActivityCallback.OnError(responseObject.message)
                    }
                }else{
                    canjesActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                canjesActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }
}