package java.app.lealtad.videogames.canjes.view

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.util.BaseActivity

class CanjesDetailsActivity : BaseActivity() {

    var tvUserD: TextView? = null
    var tvProductoD: TextView? = null
    var tvPuntosD: TextView? = null
    var tvTotalD: TextView? = null
    var tvPonitsUSerD: TextView? = null
    var tvUsuarioAddD: TextView? = null
    var tvFechaAddD: TextView? = null
    var ivhome: ImageView? = null
    var btnRegresar: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_canjes_details)
        title = "Detalle del canje"

        initializeView()

        btnRegresar!!.setOnClickListener {
            val intent= Intent(applicationContext, CanjesActivity::class.java)
            startActivity(intent)
        }

        ivhome!!.setOnClickListener {
            val intent= Intent(applicationContext, HomeActivity::class.java)
            startActivity(intent)
        }
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        ivhome = findViewById<ImageView>(R.id.ivhome)
        tvUserD = findViewById<TextView>(R.id.tvUserD)
        tvProductoD = findViewById<TextView>(R.id.tvProductoD)
        tvPuntosD = findViewById<TextView>(R.id.tvPuntosD)
        tvTotalD = findViewById<TextView>(R.id.tvTotalD)
        tvPonitsUSerD = findViewById<TextView>(R.id.tvPonitsUSerD)
        tvUsuarioAddD = findViewById<TextView>(R.id.tvUsuarioAddD)
        tvFechaAddD = findViewById<TextView>(R.id.tvFechaAddD)
        btnRegresar = findViewById<Button>(R.id.btnRegresar)

        showDetail()
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun showDetail(){
        val intent = intent
        val user = intent.getStringExtra("USERNAME")
        val producto = intent.getStringExtra("PRODUCTO")
        val point = intent.getStringExtra("POINTS")
        val pointUser = intent.getStringExtra("POINTSUSER")
        val total = intent.getStringExtra("TOTALS")
        val asigno = intent.getStringExtra("USERADD")
        val fechaadd = intent.getStringExtra("DATEADD")

        if(user != "default value"){
            tvUserD!!.text = user
        }
        if(producto != "default value"){
            tvProductoD!!.text = producto
        }
        if(point != "default value"){
            tvPuntosD!!.text = point
        }
        if(pointUser != "default value"){
            tvTotalD!!.text = pointUser
        }
        if(total != "default value"){
            tvPonitsUSerD!!.text = total
        }
        if(asigno != "default value"){
            tvUsuarioAddD!!.text = asigno
        }
        if(fechaadd != "default value"){
            tvFechaAddD!!.text = fechaadd
        }
    }
    //endregion View
}
