package java.app.lealtad.videogames.canjes.view

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.lifecycle.Observer
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.canjes.adapter.CanjesAdapter
import java.app.lealtad.videogames.canjes.interfaces.CanjesActivityCallback
import java.app.lealtad.videogames.canjes.model.Canjes
import java.app.lealtad.videogames.canjes.viewmodel.CanjesViewModel
import java.app.lealtad.videogames.canjes.viewmodel.CanjesViewModelFactory
import java.app.lealtad.videogames.databinding.ActivityCanjesBinding
import java.app.lealtad.videogames.util.BaseActivity

class CanjesActivity : BaseActivity(), CanjesActivityCallback {

    var canjesViewModel: CanjesViewModel? = null
    var canjesAdapter: CanjesAdapter? = null
    var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_canjes)
        title = "Canjes"

        val canjesMainBinding = DataBindingUtil.setContentView<ActivityCanjesBinding>(this, R.layout.activity_canjes)
        canjesMainBinding.canjes = ViewModelProviders.of(this, CanjesViewModelFactory(this, this))
            .get(CanjesViewModel::class.java)

        canjesViewModel = ViewModelProviders.of(this, CanjesViewModelFactory(this, this)).get(CanjesViewModel::class.java)

        initializeView()

        recyclerView = findViewById<RecyclerView>(R.id.recyclerCanjes)
        canjesViewModel!!.getDataPoints().observe(this,
            Observer<List<Canjes>> { arrayList ->
                canjesAdapter = CanjesAdapter(this@CanjesActivity, arrayList, this)

                recyclerView!!.layoutManager = LinearLayoutManager(this@CanjesActivity)
                recyclerView!!.adapter = canjesAdapter
            })
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnSuccess(message: String) {
        ProgresbarFinish()
        if(message != ""){
            Toasty.success(this, message, Toast.LENGTH_SHORT, true).show()
        }
    }

    override fun OnSuccessCanjes(canjesList: MutableList<Canjes>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }

    override fun deleteCanje(idcanje: String) {
        canjesViewModel!!.deleteCanje(idcanje)
    }
    //endregion View
}
