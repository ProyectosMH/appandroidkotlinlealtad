package java.app.lealtad.videogames.users.interfaces

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.app.lealtad.videogames.login.model.ResponseObject
import java.app.lealtad.videogames.users.model.UsersResponse

interface UsersServices {
    @POST("users/getUsersList")
    fun getUsersList(): Call<List<UsersResponse>>

    @POST("users/findUserByNameList")
    fun findUserByNameList(@Header("username") nameProduct: String): Call<List<UsersResponse>>

    @POST("users/saveUsers")
    fun saveUsers(@Body user: RequestBody): Call<ResponseObject>

    @POST("users/desactiveUserById")
    fun deleteUser(@Header("iduser") iduser: String): Call<ResponseObject>

    @POST("users/changePass")
    fun changePass(@Header("username") username: String, @Header("passold") passold: String, @Header("newpass") newpass: String): Call<ResponseObject>
}