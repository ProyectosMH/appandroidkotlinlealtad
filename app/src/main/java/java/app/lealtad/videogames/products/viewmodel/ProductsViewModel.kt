package java.app.lealtad.videogames.products.viewmodel

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.app.lealtad.videogames.home.model.Products
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.products.interfaces.ProductsActivityCallback
import java.app.lealtad.videogames.products.repository.ProductsRepository
import java.app.lealtad.videogames.products.view.AddProductsActivity

class ProductsViewModel: ViewModel {

    lateinit var productsActivityCallback: ProductsActivityCallback
    lateinit var context: Context
    var productsRepo: ProductsRepository

    @SerializedName("records")
    @Expose
    private var items: ArrayList<Products>? = null

    var liveData = MutableLiveData<List<Products>>()
    var nameProducto: String = ""

    fun getDataProducts(): MutableLiveData<List<Products>>{
        productsActivityCallback.ProgresBarStar()
        liveData = productsRepo.getAllProductsMutableLiveData()
        return liveData
    }

    fun goHome(view: View){
        val intent= Intent(context, HomeActivity::class.java)
        context?.startActivity(intent)
    }

    fun addProducts(view: View){
        val intent= Intent(context, AddProductsActivity::class.java)
        context?.startActivity(intent)
    }

    val getNameProduct: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                nameProducto = editable.toString()
            }
        }

    fun findProductbyName(view: View) {
        if(!nameProducto.equals("")){
            productsActivityCallback.ProgresBarStar()
            productsRepo.findProductbyName(nameProducto)
        }else{
            productsActivityCallback.OnError("Ingresa un nombre de producto para realizar la busqueda")
        }
    }

    fun deleteProduct(idproduct: String){
        productsActivityCallback.ProgresBarStar()
        productsRepo.deleteProduct(idproduct)
    }

    constructor(productsActivityCallback: ProductsActivityCallback, context: Context) {
        this.productsActivityCallback = productsActivityCallback
        this.context = context
        productsRepo = ProductsRepository(productsActivityCallback, context)
    }
}