package java.app.lealtad.videogames.geolocalizacion.view

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentActivity

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.geolocalizacion.model.Shops
import android.view.KeyEvent
import android.view.WindowManager
import java.app.lealtad.videogames.home.view.HomeActivity

class MapsActivity : FragmentActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    var shops = ArrayList<Shops>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        initializeView()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    //region Iniciar componentes
    fun initializeView(){
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        shops.add(Shops("VideosGames Las Pulgas", "13.712146", "-89.207280", "Centro comercial las pulgas, san salvador"))
        shops.add(Shops("VideosGames Metrocentro San Salvador", "13.705676", "-89.211637", "Centro comercial metrocentro, san salvador"))
        shops.add(Shops("VideosGames Plaza Mundo", "13.697950", "-89.150967", "Centro comercial plaza mundo, soyapango"))
        shops.add(Shops("VideosGames Metrocentro Santa Ana", "13.977684", "-89.562118", "Centro comercial metrocentro santa ana, santa ana"))
        shops.add(Shops("VideosGames Metrocentro San Migel", "13.461467", "-88.164968", "Centro comercial metrocentro san miguel, san miguel"))
    }
    //endregion Iniciar componentes

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        var nombre: String
        var latitude: String
        var longitude: String
        var description: String
        val markerOptions = MarkerOptions()

        for (i in 0 until shops.size) {
            nombre = shops.get(i).nombre
            latitude = shops.get(i).latitude
            longitude = shops.get(i).longitude
            description = shops.get(i).description

            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.logomap))

            val latituded: Double?
            val longituded: Double?
            latituded = java.lang.Double.parseDouble(latitude)
            longituded = java.lang.Double.parseDouble(longitude)

            val blueMarket = LatLng(latituded, longituded)
            markerOptions.title(nombre)
            markerOptions.position(blueMarket)
            markerOptions.snippet(description)
            val m = mMap.addMarker(markerOptions)
        }

        val SanSalvador = LatLng(13.6914782, -89.2146939)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SanSalvador, 12f))
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            val home = Intent(this, HomeActivity::class.java)
            startActivityForResult(home, 0)
            finish()
            return false
        } else super.onKeyDown(keyCode, event)
    }
}
