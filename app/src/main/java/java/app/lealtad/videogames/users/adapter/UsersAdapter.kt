package java.app.lealtad.videogames.users.adapter

import android.app.Dialog
import androidx.recyclerview.widget.RecyclerView
import java.app.lealtad.videogames.R
import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.annotation.NonNull
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.databinding.UsersBinding
import java.app.lealtad.videogames.users.interfaces.UsersActivityCallback
import java.app.lealtad.videogames.users.model.UsersResponse
import java.app.lealtad.videogames.users.view.UserDetailsActivity

class UsersAdapter(private val context: Context, private val arrayList: List<UsersResponse>, private val usersActivityCallback: UsersActivityCallback) : RecyclerView.Adapter<UsersAdapter.CustomView>() {

    private var layoutInflater: LayoutInflater? = null
    private var btnSi: Button? = null
    private var btnNo: Button? = null

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): CustomView {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }

        val cardBinding = layoutInflater?.let {
            DataBindingUtil.inflate<UsersBinding>(
                layoutInflater!!,
                R.layout.users_list,
                parent,
                false
            )
        }

        return cardBinding?.let { CustomView(it) }!!
    }

    override fun onBindViewHolder(@NonNull holder: CustomView, position: Int) {
        val homeViewModel = arrayList[position]
        holder.bind(homeViewModel)

        holder.usersBinding.cvDetalle.setOnClickListener{
            val intent = Intent(context, UserDetailsActivity::class.java)
            intent.putExtra("FIRSTNAME", holder.usersBinding.users!!.firstname)
            intent.putExtra("SECONDNAME", holder.usersBinding.users!!.secondname)
            intent.putExtra("SURNAME", holder.usersBinding.users!!.surname)
            intent.putExtra("SECONDSURNAME", holder.usersBinding.users!!.secondsurname)
            intent.putExtra("EMAIL", holder.usersBinding.users!!.email)
            intent.putExtra("PHONE", holder.usersBinding.users!!.phone)
            intent.putExtra("ROLNAME", holder.usersBinding.users!!.rolname)
            intent.putExtra("USERNAME", holder.usersBinding.users!!.username)
            intent.putExtra("BORN", holder.usersBinding.users!!.dateborn)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context?.startActivity(intent)
        }

        holder.usersBinding.ivDeleteUser.setOnClickListener { view ->
            openDialogDelete(holder.usersBinding.users!!.iduser)
        }
    }

    fun openDialogDelete(iduser: String): Dialog {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.delete_dialog)
        dialog.show()
        dialog.setCanceledOnTouchOutside(false)

        btnNo = dialog.findViewById(R.id.btnNo) as Button
        btnSi = dialog.findViewById(R.id.btnSi) as Button

        btnNo!!.setOnClickListener { dialog.dismiss() }

        btnSi!!.setOnClickListener {
            dialog.dismiss()
            usersActivityCallback.deleteUser(iduser)
        }

        return dialog
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class CustomView(var usersBinding: UsersBinding) : RecyclerView.ViewHolder(usersBinding.root) {

        fun bind(users: UsersResponse) {
            this.usersBinding.users = users
            usersBinding.executePendingBindings()
        }
    }
}
