package java.app.lealtad.videogames.products.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.app.lealtad.videogames.points.interfaces.PointsActivityCallback
import java.app.lealtad.videogames.products.interfaces.ProductsActivityCallback

class AddProductsViewModelFactory (private val productsActivityCallback: ProductsActivityCallback, private  val context: Context) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AddProductsViewModel(productsActivityCallback, context) as T
    }
}
