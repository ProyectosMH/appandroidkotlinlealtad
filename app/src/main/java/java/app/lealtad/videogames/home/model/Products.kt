package java.app.lealtad.videogames.home.model

import android.graphics.Bitmap
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import java.io.Serializable

class Products() : Serializable {

    lateinit var idproduct: String
    lateinit var nameproduct: String
    lateinit var price: String
    var points: String = "0"
    var totalPuntosUser: String = "0"
    lateinit var quantity: String
    lateinit var description: String
    var image: String = ""
    var promocion: String = ""
    //var imageByte: ByteArray? = null
    @Transient
    var imagen: Bitmap? = null
    var canjear: Boolean = false
    var deleteProduct: Boolean = false
    var promo: Boolean = false

    constructor(idProduct: String, nameProduct: String, Price: String, Points: String, Quantity: String, description: String, image: String, promocion: String) : this() {
        this.idproduct = idProduct
        this.nameproduct = nameProduct
        this.price = Price
        this.points = Points
        this.quantity = Quantity
        this.description = description
        this.image = image
        this.promocion = promocion
    }

    @BindingAdapter("android:imageBitmap")
    fun loadImage(imageView: ImageView, bitmap: Bitmap) {
        imageView.setImageBitmap(bitmap)
    }
}