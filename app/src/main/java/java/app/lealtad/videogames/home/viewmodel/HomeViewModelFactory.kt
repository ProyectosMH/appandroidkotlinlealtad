package java.app.lealtad.videogames.home.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.app.lealtad.videogames.home.interfaces.HomeActivityCallback
import java.app.lealtad.videogames.home.model.Products

class HomeViewModelFactory(private val homeActivityCallback: HomeActivityCallback, private  val context: Context) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return HomeViewModel(homeActivityCallback, context) as T
    }
}
