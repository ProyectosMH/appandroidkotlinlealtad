package java.app.lealtad.videogames.login.view

import android.animation.Animator
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.CountDownTimer
import android.view.KeyEvent
import android.view.View.VISIBLE
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputEditText
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_main.*
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.databinding.ActivityMainBinding
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.login.interfaces.EventsActivityCallback
import java.app.lealtad.videogames.login.viewmodel.LoginViewModel
import java.app.lealtad.videogames.login.viewmodel.LoginViewModelFactory
import java.app.lealtad.videogames.util.BaseActivity

class MainActivity : BaseActivity(), EventsActivityCallback {
    var milisecon: Long = 0
    var interval: Long = 0
    var isLogout = ""
    lateinit var etUser: TextInputEditText
    lateinit var etPassword: TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_main)

        val activityMainBinding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        activityMainBinding.viewModel = ViewModelProviders.of(this, LoginViewModelFactory(this, this))
            .get(LoginViewModel::class.java)

        isLogout = getValueSession(LOGOUT_S, applicationContext, LOGOUT_PREFERENCES)

        milisecon = when (isLogout) {
            "default value" -> 2000
            else -> 1
        }

        interval = when (isLogout) {
            "default value" -> 1000
            else -> 1
        }

        object : CountDownTimer(milisecon, interval) {
            override fun onFinish() {
                rootView.setBackgroundColor(ContextCompat.getColor(this@MainActivity, R.color.black_effective))
                IconImageView.setImageResource(R.drawable.logovg)
                startAnimation()
            }

            override fun onTick(p0: Long) {}
        }.start()

        initializeView()
    }

    //region Iniciar componentes
    fun initializeView(){
        deleteSession()
        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        etUser = findViewById<TextInputEditText>(R.id.etUser)
        etPassword = findViewById<TextInputEditText>(R.id.etPassword)
        val intent = intent
        val sesion = intent.getStringExtra("SESION")
        if(sesion =="Tu sesión ha expirado"){
            Toasty.error(this, sesion, Toast.LENGTH_SHORT, true).show()
        }
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnSuccess() {
        ProgresbarFinish()
        val intent = Intent(this@MainActivity, HomeActivity::class.java)
        startActivity(intent)
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        etUser.setText("")
        etPassword.setText("")
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }

    private fun startAnimation() {
        var duracion: Long = when (isLogout) {
            "default value" -> 2000
            else -> 1
        }
        IconImageView.animate().apply {
            y(-300f)
            duration = duracion
        }.setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) {

            }

            override fun onAnimationEnd(p0: Animator?) {
                activityMain.visibility = VISIBLE
                deleteLogout()
                fingerEnabled()
            }

            override fun onAnimationCancel(p0: Animator?) {

            }

            override fun onAnimationStart(p0: Animator?) {

            }
        })
    }
    //endregion View
}
