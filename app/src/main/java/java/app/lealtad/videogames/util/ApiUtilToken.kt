package java.app.lealtad.videogames.util

import okhttp3.Interceptor
import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiUtilToken {
    private  val BaseUrl = "http://videosgames.cool/videoGameApi/api/"
    private var retrofit: Retrofit? = null

    val apiUtil: Retrofit?
        get() {
            if(retrofit == null){
                retrofit = Retrofit.Builder()
                    .baseUrl(BaseUrl).client(requestHeader)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }

            return retrofit
        }

    private val requestHeader: OkHttpClient
        get() = OkHttpClient.Builder()
            .addInterceptor(ServiceInterceptor())
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()
}

class ServiceInterceptor : BaseActivity(), Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        if(request.header("No-Authentication") == null){
            val token = getToken()
            if(!token.isNullOrEmpty())
            {
                request = request.newBuilder()
                    .addHeader("Authorization",token)
                    .build()
            }

        }

        return chain.proceed(request)
    }
}