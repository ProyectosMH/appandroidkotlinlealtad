package java.app.lealtad.videogames.points.viewmodel

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.points.model.PointsResponse
import java.app.lealtad.videogames.points.interfaces.PointsActivityCallback
import java.app.lealtad.videogames.points.model.Points
import java.app.lealtad.videogames.points.repository.PointsRepository
import java.app.lealtad.videogames.points.view.AddPointsActivity

class AddPointsViewModel: ViewModel {

    lateinit var pointsActivityCallback: PointsActivityCallback
    lateinit var context: Context
    var pointRepo: PointsRepository
    var userName: String = ""
    var fullName: String = ""
    var codeFactura: String = ""
    var descripcion: String = ""
    var puntos: String = ""
    var idUserPoints: String = ""

    fun goHome(view: View){
        val intent= Intent(context, HomeActivity::class.java)
        context?.startActivity(intent)
    }

    val getNameUser: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                userName = editable.toString()
            }
        }

    val getDescripcion: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                descripcion = editable.toString()
            }
        }

    val getPuntos: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                puntos = editable.toString()
            }
        }

    val getIdUserPoints: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                idUserPoints = editable.toString()
            }
        }

    val getCodeFactura: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                codeFactura = editable.toString()
            }
        }

    fun savePoints(view: View){
        if(idUserPoints.equals("")){
            pointsActivityCallback.OnError("Campo usuario es requerido")
        } else if(codeFactura.equals("")){
            pointsActivityCallback.OnError("Campo código factura es requerido")
        } else if(puntos.equals("")){
            pointsActivityCallback.OnError("Campo puntos es requerido")
        } else if(descripcion.equals("")){
            pointsActivityCallback.OnError("Campo descripción es requerido")
        }else{
            pointsActivityCallback.ProgresBarStar()
            var puntosInt = Integer.parseInt(puntos)
            var idUserPonitsInt = Integer.parseInt(idUserPoints)
            val point = Points(0, idUserPonitsInt, puntosInt, 0, codeFactura, descripcion, 0, "", 0, "")
            pointRepo.addPoints(point)
        }
    }

    fun findProductbyName(view: View) {
        if(!userName.equals("")){
            pointsActivityCallback.ProgresBarStar()
            pointRepo.findProductbyName(userName)
        }else{
            pointsActivityCallback.OnError("Ingresa un usuario para realizar la busqueda")
        }
    }

    constructor(pointsActivityCallback: PointsActivityCallback, context: Context) {
        this.pointsActivityCallback = pointsActivityCallback
        this.context = context
        pointRepo = PointsRepository(pointsActivityCallback, context)
    }
}