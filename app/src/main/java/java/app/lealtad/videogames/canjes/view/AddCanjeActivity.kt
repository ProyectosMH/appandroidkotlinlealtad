package java.app.lealtad.videogames.canjes.view

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.canjes.interfaces.CanjesActivityCallback
import java.app.lealtad.videogames.canjes.model.Canjes
import java.app.lealtad.videogames.canjes.viewmodel.AddCanjeViewModel
import java.app.lealtad.videogames.canjes.viewmodel.AddCanjeViewModelFactory
import java.app.lealtad.videogames.databinding.ActivityAddCanjeBinding
import java.app.lealtad.videogames.util.BaseActivity
import android.widget.ImageView
import com.google.zxing.integration.android.IntentIntegrator
import android.content.Intent
import android.view.View
import android.widget.TextView

class AddCanjeActivity : BaseActivity(), CanjesActivityCallback {

    var ivQR: ImageView? = null
    var llReady: LinearLayout? = null
    var tvUserName: TextView? = null
    var tvProductName: TextView? = null
    var tvPuntosUser: TextView? = null
    var tvPuntosProduct: TextView? = null
    var addCanjeViewModel: AddCanjeViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_add_canje)
        title = "Canjear"

        val canjesMainBinding = DataBindingUtil.setContentView<ActivityAddCanjeBinding>(this, R.layout.activity_add_canje)
        canjesMainBinding.canje = ViewModelProviders.of(this, AddCanjeViewModelFactory(this, this))
            .get(AddCanjeViewModel::class.java)

        addCanjeViewModel = ViewModelProviders.of(this, AddCanjeViewModelFactory(this, this)).get(AddCanjeViewModel::class.java)

        initializeView()

        ivQR!!.setOnClickListener { leerQR() }
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        llReady = findViewById<LinearLayout>(R.id.llReady)
        tvUserName = findViewById<TextView>(R.id.tvUserName)
        tvProductName = findViewById<TextView>(R.id.tvProductName)
        tvPuntosUser = findViewById<TextView>(R.id.tvPuntosUser)
        tvPuntosProduct = findViewById<TextView>(R.id.tvPuntosProduct)
        ivQR = findViewById<ImageView>(R.id.ivQR)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnSuccess (message: String) {
        ProgresbarFinish()
        if(message != ""){
            if(message == "Canje Save"){
                Toasty.success(this, "Canje realizado exitosamente", Toast.LENGTH_SHORT, true).show()
                val intent= Intent(applicationContext, CanjesActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun OnSuccessCanjes(canjesList: MutableList<Canjes>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }

    fun leerQR() {
        val intent = IntentIntegrator(this)
        intent.setBeepEnabled(false)
        intent.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES)

        intent.setPrompt("Escaneando código QR")
        intent.setCameraId(0)
        intent.initiateScan()
    }

    override fun deleteCanje(idcanje: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toasty.error(this, "Cancelaste el escaneo", Toast.LENGTH_SHORT, true).show()
            } else {
                result.contents.toString()
                var textoDes: String = desencriptar(result.contents.toString())
                var resutado = textoDes.split("~")
                if(resutado.count() == 6){
                    addCanjeViewModel!!.setvalues(resutado[0], resutado[1], resutado[2], resutado[3], resutado[4], resutado[5])
                    llReady!!.visibility = View.VISIBLE
                    tvUserName!!.text = resutado[1]
                    tvProductName!!.text = resutado[3]
                    tvPuntosUser!!.text = resutado[5]
                    tvPuntosProduct!!.text = resutado[4]
                }else{
                    llReady!!.visibility = View.GONE
                    Toasty.error(this, "Codigo QR no reconocido", Toast.LENGTH_SHORT, true).show()
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
    //endregion View
}
