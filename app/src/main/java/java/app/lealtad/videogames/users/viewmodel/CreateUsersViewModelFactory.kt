package java.app.lealtad.videogames.users.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.app.lealtad.videogames.users.interfaces.UsersActivityCallback

class CreateUsersViewModelFactory (private val usersActivityCallback: UsersActivityCallback, private  val context: Context) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CreateUsersViewModel(usersActivityCallback, context) as T
    }
}
