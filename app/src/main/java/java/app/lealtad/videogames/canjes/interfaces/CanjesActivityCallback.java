package java.app.lealtad.videogames.canjes.interfaces;

import java.app.lealtad.videogames.canjes.model.Canjes;
import java.app.lealtad.videogames.home.model.Products;
import java.util.List;

public interface CanjesActivityCallback {
    void OnSuccess(String message);
    void OnSuccessCanjes(List<Canjes> canjesList);
    void OnError(String message);
    void ProgresBarStar();
    void deleteCanje(String idcanje);
}
