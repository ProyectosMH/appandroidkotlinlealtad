package java.app.lealtad.videogames.login.repository

import android.content.Context
import android.content.Intent
import androidx.collection.ArrayMap
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.app.lealtad.videogames.login.interfaces.EventsActivityCallback
import java.app.lealtad.videogames.login.interfaces.LoginService
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.util.ApiUtil
import org.json.JSONObject
import okhttp3.RequestBody
import java.app.lealtad.videogames.login.model.LoginResponse
import java.app.lealtad.videogames.login.model.ResponseObject
import java.app.lealtad.videogames.login.view.MainActivity
import java.app.lealtad.videogames.util.BaseActivity


class LoginRepository(private val loginActivityCallback: EventsActivityCallback) : BaseActivity() {

    fun getLoginUser(user: Users) {
        val jsonParams = ArrayMap<String, Any>()
        jsonParams.put("user", user.username)
        jsonParams.put("password", user.pass)

        val body = RequestBody.create(
            okhttp3.MediaType.parse("application/json; charset=utf-8"),
            JSONObject(jsonParams).toString()
        )

        var service = ApiUtil.apiUtil?.create(LoginService::class.java)
        val call = service?.getLoginUser(body)
        var userResponse: JsonObject

        call?.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.code() == 200) {
                    userResponse = response.body()!!
                    loginActivityCallback.OnSuccess()
                }else{
                    loginActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                loginActivityCallback.OnError("no sirve nada")
            }
        })
    }

    fun getTokenValidate(user: Users, context: Context) {
        user.username = encriptar(user.username.toString())
        user.pass = encriptar(user.pass.toString())
        var service = ApiUtil.apiUtil?.create(LoginService::class.java)
        val call = service?.getTokenValidate(user.username.toString(), user.pass.toString())
        var userResponse: LoginResponse

        call?.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if (response.code() == 200) {
                    userResponse = response.body()!!
                    if(userResponse.username.equals("Usuario o contraseña incorrecta")){
                        user.username = ""
                        user.pass = ""
                        loginActivityCallback.OnError("Usuario o contraseña incorrecta")
                    }else{
                        setValueSession(USER_S, userResponse.username.toString(), context, SESSION_PREFERENCES)
                        setValueSession(PASS_S, user.pass.toString(), context, SESSION_PREFERENCES)
                        setValueSession(IDUSER_S, userResponse.iduser.toString(), context, SESSION_PREFERENCES)
                        setValueSession(ROLUSER_S, userResponse.idrol .toString(), context, SESSION_PREFERENCES)
                        setValueSession(TOKEN_S, userResponse.token.toString(), context, SESSION_PREFERENCES)
                        setValueSession(DATEBORN_S, userResponse.dateborn.toString(), context, SESSION_PREFERENCES)
                        setValueSession(DATEBORN_S, userResponse.dateborn.toString(), context, CUMPLE_PREFERENCES)
                        loginActivityCallback.OnSuccess()
                    }
                }else{
                    loginActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                loginActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }

    fun goHome(context: Context){
        setValueSession(LOGOUT_S, "logout", context, LOGOUT_PREFERENCES)
        val intent= Intent(context, MainActivity::class.java)
        context?.startActivity(intent)
        finish()
    }

    fun recoveryPass(username: String, phone: String, email: String, newPass: String){
        var service = ApiUtil.apiUtil?.create(LoginService::class.java)
        val call = service?.passForget(username, email, phone, newPass)
        var responseObject: ResponseObject

        call?.enqueue(object : Callback<ResponseObject> {
            override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                if (response.code() == 200) {
                    responseObject = response.body()!!
                    if(responseObject.message == "Contraseña actualizada exitosamente"){
                        loginActivityCallback.OnSuccess()
                    }else{
                        loginActivityCallback.OnError(responseObject.message)
                    }
                }else {
                    loginActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                loginActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }
}