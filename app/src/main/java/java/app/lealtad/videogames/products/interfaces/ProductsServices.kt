package java.app.lealtad.videogames.products.interfaces

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.app.lealtad.videogames.home.model.Products
import java.app.lealtad.videogames.login.model.ResponseObject
import java.app.lealtad.videogames.points.model.PointsResponse

interface ProductsServices {
    @POST("products/findProductbyName")
    fun findProductbyName(@Header("nameProduct") nameProduct: String): Call<List<Products>>

    @POST("products/saveProduct")
    fun saveProduct(@Body product: RequestBody): Call<ResponseObject>

    @POST("products/editProduct")
    fun editProduct(@Body product: RequestBody): Call<ResponseObject>

    @POST("products/deleteProductsById")
    fun deleteProduct(@Header("idproduct") idproduct: String): Call<ResponseObject>
}