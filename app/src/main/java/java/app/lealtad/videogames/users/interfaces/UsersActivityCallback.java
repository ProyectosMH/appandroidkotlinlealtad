package java.app.lealtad.videogames.users.interfaces;

import java.app.lealtad.videogames.login.model.Users;
import java.app.lealtad.videogames.users.model.UsersResponse;
import java.util.List;

public interface UsersActivityCallback {
    void OnSuccess(String message);
    void OnError(String message);
    void ProgresBarStar();
    void ValidateFragmentOne(Users user);
    void ValidateFragmentTwo(Users user);
    void OnSuccessUsers(List<UsersResponse> usersList);
    void backOneFragment(Users user);
    void deleteUser(String iduser);
}

