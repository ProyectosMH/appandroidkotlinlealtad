package java.app.lealtad.videogames.users.view

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.util.BaseActivity

class UserDetailsActivity : BaseActivity() {

    var tvNamesD: TextView? = null
    var tvSurnamesD: TextView? = null
    var tvUsuarioD: TextView? = null
    var tvEmailD: TextView? = null
    var tvPhoneD: TextView? = null
    var tvRolD: TextView? = null
    var tvBordnD: TextView? = null
    var ivhome: ImageView? = null
    var btnRegresar: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_user_details)
        title = "Detalle del usuario"

        initializeView()

        btnRegresar!!.setOnClickListener {
            val intent= Intent(applicationContext, UsersActivity::class.java)
            startActivity(intent)
        }

        ivhome!!.setOnClickListener {
            val intent= Intent(applicationContext, HomeActivity::class.java)
            startActivity(intent)
        }
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        ivhome = findViewById<ImageView>(R.id.ivhome)
        tvNamesD = findViewById<TextView>(R.id.tvNamesD)
        tvSurnamesD = findViewById<TextView>(R.id.tvSurnamesD)
        tvUsuarioD = findViewById<TextView>(R.id.tvUsuarioD)
        tvEmailD = findViewById<TextView>(R.id.tvEmailD)
        tvPhoneD = findViewById<TextView>(R.id.tvPhoneD)
        tvRolD = findViewById<TextView>(R.id.tvRolD)
        tvBordnD = findViewById<TextView>(R.id.tvBordnD)
        btnRegresar = findViewById<Button>(R.id.btnRegresar)

        showDetail()
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun showDetail(){
        val intent = intent
        val name = intent.getStringExtra("FIRSTNAME")
        val secondName = intent.getStringExtra("SECONDNAME")
        val surname = intent.getStringExtra("SURNAME")
        val secondSurname = intent.getStringExtra("SECONDSURNAME")
        val email = intent.getStringExtra("EMAIL")
        val phone = intent.getStringExtra("PHONE")
        val rol = intent.getStringExtra("ROLNAME")
        val username = intent.getStringExtra("USERNAME")
        val born = intent.getStringExtra("BORN")

        if(name != "default value"){
            tvNamesD!!.text = name + " " + secondName
        }
        if(surname != "default value"){
            tvSurnamesD!!.text = surname + " " + secondSurname
        }
        if(username != "default value"){
            tvUsuarioD!!.text = username
        }
        if(email != "default value"){
            tvEmailD!!.text = email
        }
        if(phone != "default value"){
            tvPhoneD!!.text = phone
        }
        if(rol != "default value"){
            tvRolD!!.text = rol
        }
        if(born != "default value"){
            tvBordnD!!.text = born
        }
    }
    //endregion View
}
