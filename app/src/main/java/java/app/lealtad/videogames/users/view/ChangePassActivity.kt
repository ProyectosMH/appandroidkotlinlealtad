package java.app.lealtad.videogames.users.view

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.databinding.ActivityChangePassBinding
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.users.interfaces.UsersActivityCallback
import java.app.lealtad.videogames.users.model.UsersResponse
import java.app.lealtad.videogames.users.viewmodel.UsersViewModel
import java.app.lealtad.videogames.users.viewmodel.UsersViewModelFactory
import java.app.lealtad.videogames.util.BaseActivity

class ChangePassActivity : BaseActivity(), UsersActivityCallback {

    var usersViewModel: UsersViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_change_pass)
        title = "Cambiar Contraseña"

        val usersMainBinding = DataBindingUtil.setContentView<ActivityChangePassBinding>(this, R.layout.activity_change_pass)
        usersMainBinding.users = ViewModelProviders.of(this, UsersViewModelFactory(this, this))
            .get(UsersViewModel::class.java)

        usersViewModel = ViewModelProviders.of(this, UsersViewModelFactory(this, this)).get(
            UsersViewModel::class.java)

        initializeView()
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnSuccess(message: String) {
        ProgresbarFinish()
        if(message != ""){
            Toasty.success(this, message, Toast.LENGTH_SHORT, true).show()
        }
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun OnSuccessUsers(usersList: List<UsersResponse>) {
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }

    override fun ValidateFragmentOne(user: Users) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ValidateFragmentTwo(user: Users) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun backOneFragment(user: Users?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteUser(iduser: String) {
    }
    //endregion View
}
