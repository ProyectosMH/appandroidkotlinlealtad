package java.app.lealtad.videogames.home.interfaces

import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST
import java.app.lealtad.videogames.points.model.PointsResponse
import java.app.lealtad.videogames.home.model.Products

interface HomeServices {
    @POST("points/getAllPoint")
    fun LoadPoints(@Header("idUser") idUser: String): Call<PointsResponse>

    @POST("products/getAllProducts")
    fun getAllProducts(@Header("promocion") promocion: String): Call<List<Products>>

    @POST("products/getListProducts")
    fun getListProducts(): Call<List<Products>>
}