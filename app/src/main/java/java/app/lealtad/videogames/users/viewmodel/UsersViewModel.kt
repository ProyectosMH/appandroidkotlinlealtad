package java.app.lealtad.videogames.users.viewmodel

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.users.interfaces.UsersActivityCallback
import java.app.lealtad.videogames.users.model.UsersResponse
import java.app.lealtad.videogames.users.repository.UsersRepository
import java.app.lealtad.videogames.users.view.CreateUserActivity

class UsersViewModel: ViewModel {

    lateinit var usersActivityCallback: UsersActivityCallback
    lateinit var context: Context
    var userRepo: UsersRepository

    @SerializedName("records")
    @Expose
    private var items: ArrayList<UsersResponse>? = null
    var userName: String = ""
    var passActual: String = ""
    var newPass: String = ""
    var confirmNewPass: String = ""

    var liveData = MutableLiveData<List<UsersResponse>>()

    val getNameUser: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                userName = editable.toString()
            }
        }

    val getPassActText: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                passActual = editable.toString()
            }
        }

    val getNewPassText: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                newPass = editable.toString()
            }
        }

    val getConfNewPassText: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                confirmNewPass = editable.toString()
            }
        }

    fun goHome(view: View){
        val intent= Intent(context, HomeActivity::class.java)
        context?.startActivity(intent)
    }

    fun changePass(view: View){
        if(!passActual.equals("")){
            if(!newPass.equals("")){
                if(!confirmNewPass.equals("")){
                    if(newPass == confirmNewPass){
                        usersActivityCallback.ProgresBarStar()
                        userRepo.changePass(passActual, newPass)
                    }else{
                        usersActivityCallback.OnError("Las contraseñas no coinciden")
                    }
                }else{
                    usersActivityCallback.OnError("Ingresa la confirmacion de la contraseña")
                }
            }else{
                usersActivityCallback.OnError("Ingresa la nueva contraseña")
            }
        }else{
            usersActivityCallback.OnError("Ingresa la contraseña actual")
        }
    }

    fun findProductbyName(view: View) {
        if(!userName.equals("")){
            usersActivityCallback.ProgresBarStar()
            userRepo.findProductbyName(userName)
        }else{
            usersActivityCallback.OnError("Ingresa un usuario para realizar la busqueda")
        }
    }

    fun createUser(view: View) {
        val intent= Intent(context, CreateUserActivity::class.java)
        context?.startActivity(intent)
    }

    fun deleteUser(iduser: String){
        usersActivityCallback.ProgresBarStar()
        userRepo.deleteUser(iduser)
    }

    fun getDataUsers(): MutableLiveData<List<UsersResponse>> {
        usersActivityCallback.ProgresBarStar()
        liveData = userRepo.getUsersMutableLiveData()
        usersActivityCallback.OnSuccess("")
        return liveData
    }

    constructor(usersActivityCallback: UsersActivityCallback, context: Context) {
        this.usersActivityCallback = usersActivityCallback
        this.context = context
        userRepo = UsersRepository(usersActivityCallback, context)
    }
}