package java.app.lealtad.videogames.points.repository

import android.content.Context
import androidx.collection.ArrayMap
import androidx.lifecycle.MutableLiveData
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.app.lealtad.videogames.login.model.ResponseObject
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.points.model.PointsResponse
import java.app.lealtad.videogames.points.interfaces.PointsActivityCallback
import java.app.lealtad.videogames.points.interfaces.PointsServices
import java.app.lealtad.videogames.points.model.Points
import java.app.lealtad.videogames.util.ApiUtilToken
import java.app.lealtad.videogames.util.BaseActivity
import java.text.SimpleDateFormat
import java.util.*
import android.content.Intent
import java.app.lealtad.videogames.points.view.PointsActivity


class PointsRepository(private val pointsActivityCallback: PointsActivityCallback, private val context: Context) : BaseActivity() {

    fun getPointsMutableLiveData(): MutableLiveData<List<PointsResponse>> {
        var idUser = getValueSession(IDUSER_S, context, SESSION_PREFERENCES)
        var idRol = getValueSession(ROLUSER_S, context, SESSION_PREFERENCES)
        var liveData = MutableLiveData<List<PointsResponse>>()
        var service = ApiUtilToken.apiUtil?.create(PointsServices::class.java)
        val call = service?.getPointsList(idUser, idRol)

        call?.enqueue(object : Callback<List<PointsResponse>> {
            override fun onResponse(call: Call<List<PointsResponse>>, response: Response<List<PointsResponse>>) {
                if (response.code() == 200) {
                    liveData.value = response.body()!!
                    liveData.value!!.forEach {
                        if(idRol != "3"){
                            if(it.codefactura != "Canje producto" && it.codefactura != "Eliminacion de puntos"){
                                it.showDeleteButon =  true
                            }
                        }
                    }
                    pointsActivityCallback.OnSuccess("")
                }else{
                    pointsActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<List<PointsResponse>>, t: Throwable) {
                liveData.value = null
            }
        })

        return liveData
    }

    fun flotingShow(): Boolean{
        var isnotComunUSer: Boolean = false
        var idUser: String = getValueSession(ROLUSER_S, context, SESSION_PREFERENCES)
        if(idUser != "3"){
            isnotComunUSer = true
        }
        return isnotComunUSer
    }

    fun addPoints(points: Points) {
        val jsonParams = ArrayMap<String, Any>()
        var userAddSession = getValueSession(IDUSER_S, context, SESSION_PREFERENCES)
        val currentDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        jsonParams.put("idpoint", points.idpoint)
        jsonParams.put("iduser", points.iduser)
        jsonParams.put("codefactura", points.codefactura)
        jsonParams.put("points", points.points)
        jsonParams.put("totals", points.totals)
        jsonParams.put("description", points.description)
        jsonParams.put("iduseradd", userAddSession)
        jsonParams.put("dateadd", currentDate)
        jsonParams.put("idusermod", points.idusermod)
        jsonParams.put("datemod", points.datemod)

        val body = RequestBody.create(
            okhttp3.MediaType.parse("application/json; charset=utf-8"),
            JSONObject(jsonParams).toString()
        )

        var service = ApiUtilToken.apiUtil?.create(PointsServices::class.java)
        val call = service?.addPoints(body)
        var responseObject: ResponseObject

        call?.enqueue(object : Callback<ResponseObject> {
            override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                if (response.code() == 200) {
                    responseObject = response.body()!!
                    if(responseObject.success){
                        responseObject = response.body()!!
                        pointsActivityCallback.OnSuccess("Save point")
                    }else {
                        pointsActivityCallback.OnError(responseObject.message)
                    }
                }else{
                    pointsActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                pointsActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }

    fun deletePoint(idpoint: String){
        var iduseradd: String = getValueSession(IDUSER_S, context, SESSION_PREFERENCES)
        var service = ApiUtilToken.apiUtil?.create(PointsServices::class.java)
        val call = service?.deletePoint(idpoint.toInt(), iduseradd.toInt())
        var responseObject: ResponseObject

        call?.enqueue(object : Callback<ResponseObject> {
            override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                if (response.code() == 200) {
                    responseObject = response.body()!!
                    if(responseObject.success){
                        responseObject = response.body()!!
                        val intent = Intent(context, PointsActivity::class.java)
                        context?.startActivity(intent)
                        finish()
                        pointsActivityCallback.OnSuccess(responseObject.message)
                    }else {
                        pointsActivityCallback.OnError(responseObject.message)
                    }
                }else{
                    pointsActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                pointsActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }

    fun findProductbyName(userName: String) {
        var service = ApiUtilToken.apiUtil?.create(PointsServices::class.java)
        val call = service?.findUserByName(userName)
        var user: Users

        call?.enqueue(object : Callback<Users> {
            override fun onResponse(call: Call<Users>, response: Response<Users>) {
                if (response.code() == 200) {
                    user = response.body()!!
                    if(user.firstname != null){
                        user = response.body()!!
                        pointsActivityCallback.LoadUserByName(user)
                    }else {
                        pointsActivityCallback.OnError("Usuario no encontrado")
                    }
                }else{
                    pointsActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<Users>, t: Throwable) {
                pointsActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }
}