package java.app.lealtad.videogames.products.view

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.databinding.ActivityAddProductsBinding
import java.app.lealtad.videogames.home.model.Products
import java.app.lealtad.videogames.products.interfaces.ProductsActivityCallback
import java.app.lealtad.videogames.products.viewmodel.AddProductsViewModel
import java.app.lealtad.videogames.products.viewmodel.AddProductsViewModelFactory
import java.app.lealtad.videogames.util.BaseActivity
import java.io.File
import android.provider.MediaStore
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.os.Build
import android.util.Base64
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputLayout
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner
import java.io.ByteArrayOutputStream

class AddProductsActivity : BaseActivity(), ProductsActivityCallback {

    var ivImageProduct: ImageView? = null
    var addProductsViewModel: AddProductsViewModel? = null
    var MyVersion: Int = Build.VERSION.SDK_INT
    var SPINNERLIST = arrayOf("Si", "No")
    var materialDesignSpinner: MaterialBetterSpinner? = null
    var isPromo: String = ""
    var tilPuntos: TextInputLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_add_products)
        title = "Agregar Productos"

        val pointsMainBinding = DataBindingUtil.setContentView<ActivityAddProductsBinding>(this, R.layout.activity_add_products)
        pointsMainBinding.product = ViewModelProviders.of(this, AddProductsViewModelFactory(this, this))
            .get(AddProductsViewModel::class.java)

        addProductsViewModel = ViewModelProviders.of(this, AddProductsViewModelFactory(this, this)).get(AddProductsViewModel::class.java)

        initializeView()

        ivImageProduct!!.setOnClickListener {
            if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (!checkIfAlreadyhavePermission()) {
                    val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        this.requestPermissions(permissions, 5)
                    }
                }else{
                    openCamera()
                }
            }
        }

        materialDesignSpinner!!.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, position, l ->
                isPromo = adapterView.getItemAtPosition(position).toString()
                addProductsViewModel!!.isPromocion(isPromo)
            }
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        ivImageProduct = findViewById<ImageView>(R.id.ivImageProduct)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, SPINNERLIST)
        materialDesignSpinner = findViewById(R.id.msPromo) as MaterialBetterSpinner
        materialDesignSpinner!!.setAdapter<ArrayAdapter<String>>(arrayAdapter)
        tilPuntos = findViewById<TextInputLayout>(R.id.tilPuntos)
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    private fun checkIfAlreadyhavePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(
                        this@AddProductsActivity,
                        "Por favor acepta los permisos para poder cargar la imagen de la memoria.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    fun openCamera() {
        val i = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(i, 2)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode === 2 && resultCode === Activity.RESULT_OK) {
            val imageUri = data!!.data
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

            val cursor = contentResolver.query(imageUri, filePathColumn, null, null, null)
            cursor!!.moveToFirst()

            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
            val picturePath = cursor.getString(columnIndex)
            cursor.close()

            val file = File(picturePath)
            val length = file.length() / 1024
            if(length > 100){
                Toasty.error(this, "La imagen es muy pesada, por favor selecciona otra imagen", Toast.LENGTH_SHORT, true).show()
            }else{
                val baos: ByteArrayOutputStream = ByteArrayOutputStream()
                val bitmap = BitmapFactory.decodeFile(picturePath)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                var imageString: String = Base64.encodeToString(baos.toByteArray(), Base64.NO_WRAP)
                addProductsViewModel!!.setImagenBase64(imageString)
                ivImageProduct!!.setImageURI(imageUri)
            }
        }
    }

    override fun OnSuccess(message: String) {
        ProgresbarFinish()
        if(!message.equals("")){
            Toasty.success(this, message, Toast.LENGTH_SHORT, true).show()
            val intent= Intent(applicationContext, ProductsActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun OnSuccessProducts(productsList: List<Products>) {
        ProgresbarFinish()
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }

    override fun deleteProduct(idproduct: String) {
    }

    override fun isPromotion(promotion: Boolean) {
        if(promotion){
            tilPuntos!!.visibility = View.GONE
        }else{
            tilPuntos!!.visibility = View.VISIBLE
        }
    }
    //endregion View
}
