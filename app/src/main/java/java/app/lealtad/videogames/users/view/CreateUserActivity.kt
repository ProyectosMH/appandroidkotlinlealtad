package java.app.lealtad.videogames.users.view

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.databinding.ActivityCreateUserBinding
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.users.interfaces.UsersActivityCallback
import java.app.lealtad.videogames.users.model.UsersResponse
import java.app.lealtad.videogames.users.viewmodel.CreateUsersViewModel
import java.app.lealtad.videogames.users.viewmodel.CreateUsersViewModelFactory
import java.app.lealtad.videogames.util.BaseActivity

class CreateUserActivity : BaseActivity(), UsersActivityCallback {

    var userAdmin: String = ""
    var createUserViewModel: CreateUsersViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_create_user)
        title = "Crear Usuario"

        val createUsersMainBinding = DataBindingUtil.setContentView<ActivityCreateUserBinding>(this, R.layout.activity_create_user)
        createUsersMainBinding.user = ViewModelProviders.of(this, CreateUsersViewModelFactory(this, this))
            .get(CreateUsersViewModel::class.java)

        createUserViewModel = ViewModelProviders.of(this, CreateUsersViewModelFactory(this, this)).get(CreateUsersViewModel::class.java)

        initializeView()
    }

    //region Iniciar componentes
    fun initializeView(){
        userAdmin = getValueSession(ROLUSER_S, applicationContext, SESSION_PREFERENCES)
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        val textFragment = CreateUserOneFragment(Users())
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.fragment_container,textFragment)
        transaction.addToBackStack(null)
        transaction.commit()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnSuccess(message: String) {
        ProgresbarFinish()
        if(message != ""){
            if(message == "User Save"){
                Toasty.success(this, "Usuario creado exitosamente", Toast.LENGTH_SHORT, true).show()
                val intent= Intent(applicationContext, UsersActivity::class.java)
                startActivity(intent)
                finish()
            }else{
                Toasty.success(this, message, Toast.LENGTH_SHORT, true).show()
            }
        }
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun OnSuccessUsers(usersList: MutableList<UsersResponse>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }

    override fun ValidateFragmentOne(user: Users) {
        val userAdmin: String = getValueSession(ROLUSER_S, applicationContext, SESSION_PREFERENCES)
        val textFragment = CreateUserTwoFragment(user, userAdmin)
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.fragment_container, textFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun ValidateFragmentTwo(user: Users) {
        ProgresBarStar()
        createUserViewModel!!.saveUser(user)
    }

    override fun backOneFragment(user: Users) {
        val textFragment = CreateUserOneFragment(user)
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.fragment_container,textFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun deleteUser(iduser: String) {
    }
    //endregion View
}
