package java.app.lealtad.videogames.canjes.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.app.lealtad.videogames.canjes.interfaces.CanjesActivityCallback

class AddCanjeViewModelFactory (private val canjesActivityCallback: CanjesActivityCallback, private  val context: Context) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AddCanjeViewModel(canjesActivityCallback, context) as T
    }
}
