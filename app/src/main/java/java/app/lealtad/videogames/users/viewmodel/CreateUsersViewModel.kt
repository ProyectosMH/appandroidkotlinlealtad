package java.app.lealtad.videogames.users.viewmodel

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.users.interfaces.UsersActivityCallback
import java.app.lealtad.videogames.users.model.UsersResponse
import java.app.lealtad.videogames.users.repository.UsersRepository
import java.app.lealtad.videogames.users.view.CreateUserActivity

class CreateUsersViewModel: ViewModel {

    lateinit var usersActivityCallback: UsersActivityCallback
    lateinit var context: Context
    var userRepo: UsersRepository

    @SerializedName("records")
    @Expose
    private var items: ArrayList<UsersResponse>? = null
    var nameProducto: String = ""

    var liveData = MutableLiveData<List<UsersResponse>>()

    fun goHome(view: View){
        userRepo.goHomeLogin()
    }

    fun saveUser(user: Users){
        userRepo.saveUSer(user)
    }

    constructor(usersActivityCallback: UsersActivityCallback, context: Context) {
        this.usersActivityCallback = usersActivityCallback
        this.context = context
        userRepo = UsersRepository(usersActivityCallback, context)
    }
}