package java.app.lealtad.videogames.points.adapter

import android.app.Dialog
import android.app.PendingIntent.getActivity
import androidx.recyclerview.widget.RecyclerView
import java.app.lealtad.videogames.R
import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.NonNull
import es.dmoral.toasty.Toasty
import net.glxn.qrgen.android.QRCode
import java.app.lealtad.videogames.databinding.PointsBinding
import java.app.lealtad.videogames.points.interfaces.PointsActivityCallback
import java.app.lealtad.videogames.points.model.PointsResponse
import java.app.lealtad.videogames.points.view.PointsDetailsActivity

class PointsAdapter(private val context: Context, private val arrayList: List<PointsResponse>, private val pointsActivityCallback: PointsActivityCallback) : RecyclerView.Adapter<PointsAdapter.CustomView>() {

    private var layoutInflater: LayoutInflater? = null
    private var btnSi: Button? = null
    private var btnNo: Button? = null

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): CustomView {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }

        val cardBinding = layoutInflater?.let {
            DataBindingUtil.inflate<PointsBinding>(
                layoutInflater!!,
                R.layout.points_list,
                parent,
                false
            )
        }

        return cardBinding?.let { CustomView(it) }!!
    }

    override fun onBindViewHolder(@NonNull holder: CustomView, position: Int) {
        val homeViewModel = arrayList[position]
        holder.bind(homeViewModel)

        holder.pointsBinding.cvDetalle.setOnClickListener{
            val intent = Intent(context, PointsDetailsActivity::class.java)
            intent.putExtra("USERNAME", holder.pointsBinding.points!!.username)
            intent.putExtra("POINTS", holder.pointsBinding.points!!.points)
            intent.putExtra("TOTALS", holder.pointsBinding.points!!.totals)
            intent.putExtra("CODEFACTURA", holder.pointsBinding.points!!.codefactura)
            intent.putExtra("DESCRIPCION", holder.pointsBinding.points!!.description)
            intent.putExtra("USERADD", holder.pointsBinding.points!!.useradd)
            intent.putExtra("DATEADD", holder.pointsBinding.points!!.dateadd)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context?.startActivity(intent)
        }

        holder.pointsBinding.ivDeletepoint.setOnClickListener {
            openDialogDelete(holder.pointsBinding.points!!.idpoint)
        }
    }

    fun openDialogDelete(idpoint: String): Dialog {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.delete_dialog)
        dialog.show()
        dialog.setCanceledOnTouchOutside(false)

        btnNo = dialog.findViewById(R.id.btnNo) as Button
        btnSi = dialog.findViewById(R.id.btnSi) as Button

        btnNo!!.setOnClickListener { dialog.dismiss() }

        btnSi!!.setOnClickListener {
            dialog.dismiss()
            pointsActivityCallback.deletePoint(idpoint)
        }

        return dialog
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class CustomView(var pointsBinding: PointsBinding) : RecyclerView.ViewHolder(pointsBinding.root) {

        fun bind(points: PointsResponse) {
            this.pointsBinding.points = points
            pointsBinding.executePendingBindings()
        }
    }
}
