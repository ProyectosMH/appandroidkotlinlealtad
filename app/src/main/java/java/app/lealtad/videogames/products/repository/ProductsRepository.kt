package java.app.lealtad.videogames.products.repository

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.collection.ArrayMap
import androidx.lifecycle.MutableLiveData
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.app.lealtad.videogames.home.interfaces.HomeServices
import java.app.lealtad.videogames.home.model.Products
import java.app.lealtad.videogames.login.model.ResponseObject
import java.app.lealtad.videogames.products.interfaces.ProductsActivityCallback
import java.app.lealtad.videogames.products.interfaces.ProductsServices
import java.app.lealtad.videogames.util.ApiUtilToken
import java.app.lealtad.videogames.util.BaseActivity
import java.text.SimpleDateFormat
import java.util.*
import android.util.Base64
import java.app.lealtad.videogames.products.view.ProductsActivity

class ProductsRepository(private  val productsActivityCallback: ProductsActivityCallback, private  val context: Context) : BaseActivity() {

    fun findProductbyName(nameProduct: String) {
        var service = ApiUtilToken.apiUtil?.create(ProductsServices::class.java)
        val call = service?.findProductbyName(nameProduct)

        call?.enqueue(object : Callback<List<Products>> {
            override fun onResponse(call: Call<List<Products>>, response: Response<List<Products>>) {
                if (response.code() == 200) {
                    var productsList: List<Products> = response.body()!!
                    productsActivityCallback.OnSuccessProducts(productsList)
                }else{
                    productsActivityCallback.OnError(response.message())
                }
            }
            override fun onFailure(call: Call<List<Products>>, t: Throwable) {
                productsActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }

    fun getAllProductsMutableLiveData(): MutableLiveData<List<Products>> {
        var liveData = MutableLiveData<List<Products>>()
        var service = ApiUtilToken.apiUtil?.create(HomeServices::class.java)
        val call = service?.getListProducts()

        call?.enqueue(object : Callback<List<Products>> {
            override fun onResponse(call: Call<List<Products>>, response: Response<List<Products>>) {
                if (response.code() == 200) {
                    liveData.value = response.body()!!

                    liveData.value!!.forEach {
                        if(it.image != null){
                            if(it.image != ""){
                                val decodedString = Base64.decode(it.image, Base64.NO_WRAP)
                                it.imagen = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                                it.deleteProduct =  true
                            }
                        }
                    }
                    productsActivityCallback.OnSuccess("")
                }
            }
            override fun onFailure(call: Call<List<Products>>, t: Throwable) {
                liveData.value = null
            }
        })

        return liveData
    }

    fun getProductsMutableLiveData(): MutableLiveData<List<Products>> {
        var liveData = MutableLiveData<List<Products>>()
        var service = ApiUtilToken.apiUtil?.create(HomeServices::class.java)
        val call = service?.getAllProducts("No")

        call?.enqueue(object : Callback<List<Products>> {
            override fun onResponse(call: Call<List<Products>>, response: Response<List<Products>>) {
                if (response.code() == 200) {
                    liveData.value = response.body()!!

                    liveData.value!!.forEach {
                        if(it.image != null){
                            if(it.image != ""){
                                val decodedString = Base64.decode(it.image, Base64.NO_WRAP)
                                it.imagen = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                                it.deleteProduct =  true
                            }
                        }
                    }
                    productsActivityCallback.OnSuccess("")
                }
            }
            override fun onFailure(call: Call<List<Products>>, t: Throwable) {
                liveData.value = null
            }
        })

        return liveData
    }

    fun saveProduct(product: Products) {
        val jsonParams = ArrayMap<String, Any>()
        var userAddSession = getValueSession(IDUSER_S, context, SESSION_PREFERENCES)
        val currentDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        jsonParams.put("idproduct", 0)
        jsonParams.put("nameproduct", product.nameproduct)
        jsonParams.put("price", product.price)
        jsonParams.put("points", product.points)
        jsonParams.put("quantity", product.quantity)
        jsonParams.put("description", product.description)
        jsonParams.put("image", product.image)
        jsonParams.put("iduseradd", userAddSession)
        jsonParams.put("dateadd", currentDate)
        jsonParams.put("idusermod", 0)
        jsonParams.put("datemod", "")
        jsonParams.put("promocion", product.promocion)

        val body = RequestBody.create(
            okhttp3.MediaType.parse("application/json; charset=utf-8"),
            JSONObject(jsonParams).toString()
        )

        var service = ApiUtilToken.apiUtil?.create(ProductsServices::class.java)
        val call = service?.saveProduct(body)
        var responseObject: ResponseObject

        call?.enqueue(object : Callback<ResponseObject> {
            override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                if (response.code() == 200) {
                    responseObject = response.body()!!
                    if(responseObject.success){
                        responseObject = response.body()!!
                        productsActivityCallback.OnSuccess("Producto creado exitosamente")
                    }else {
                        productsActivityCallback.OnError(responseObject.message)
                    }
                }else{
                    productsActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                productsActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }

    fun editProduct(product: Products) {
        val jsonParams = ArrayMap<String, Any>()
        var userAddSession = getValueSession(IDUSER_S, context, SESSION_PREFERENCES)
        val currentDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        jsonParams.put("idproduct", product.idproduct)
        jsonParams.put("nameproduct", product.nameproduct)
        jsonParams.put("price", product.price)
        jsonParams.put("points", product.points)
        jsonParams.put("quantity", product.quantity)
        jsonParams.put("description", product.description)
        jsonParams.put("image", product.image)
        jsonParams.put("iduseradd", "")
        jsonParams.put("dateadd", "")
        jsonParams.put("idusermod", userAddSession)
        jsonParams.put("datemod", currentDate)
        jsonParams.put("promocion", product.promocion)

        val body = RequestBody.create(
            okhttp3.MediaType.parse("application/json; charset=utf-8"),
            JSONObject(jsonParams).toString()
        )

        var service = ApiUtilToken.apiUtil?.create(ProductsServices::class.java)
        val call = service?.editProduct(body)
        var responseObject: ResponseObject

        call?.enqueue(object : Callback<ResponseObject> {
            override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                if (response.code() == 200) {
                    responseObject = response.body()!!
                    if(responseObject.success){
                        responseObject = response.body()!!
                        productsActivityCallback.OnSuccess(responseObject.message)
                    }else {
                        productsActivityCallback.OnError(responseObject.message)
                    }
                }else{
                    productsActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                productsActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }

    fun deleteProduct(idproduct: String){
        var service = ApiUtilToken.apiUtil?.create(ProductsServices::class.java)
        val call = service?.deleteProduct(idproduct)
        var responseObject: ResponseObject

        call?.enqueue(object : Callback<ResponseObject> {
            override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                if (response.code() == 200) {
                    responseObject = response.body()!!
                    val intent = Intent(context, ProductsActivity::class.java)
                    context?.startActivity(intent)
                    finish()
                    productsActivityCallback.OnSuccess(responseObject.message)
                }else{
                    productsActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                productsActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }
}