package java.app.lealtad.videogames.canjes.interfaces

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.app.lealtad.videogames.canjes.model.Canjes
import java.app.lealtad.videogames.login.model.ResponseObject

interface CanjesServices {
    @POST("canjes/getCanjesList")
    fun getCanjesList(@Header("idUser") idUser: String, @Header("idRol") idRol: String): Call<List<Canjes>>

    @POST("canjes/saveCanje")
    fun saveCanje(@Body canjes: RequestBody): Call<ResponseObject>

    @POST("canjes/deleteCanjesById")
    fun deleteCanje(@Header("idcanje") idcanje: Int): Call<ResponseObject>
}