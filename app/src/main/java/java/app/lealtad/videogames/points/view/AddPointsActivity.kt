package java.app.lealtad.videogames.points.view

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.databinding.ActivityAddPointsBinding
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.points.interfaces.PointsActivityCallback
import java.app.lealtad.videogames.points.viewmodel.AddPointsViewModel
import java.app.lealtad.videogames.points.viewmodel.AddPointsViewModelFactory
import java.app.lealtad.videogames.points.viewmodel.PointsViewModel
import java.app.lealtad.videogames.util.BaseActivity

class AddPointsActivity : BaseActivity(), PointsActivityCallback {
    var tvFullName: TextView? = null
    var tvidUserPoint: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_add_points)
        title = "Agregar Puntos"

        val pointsMainBinding = DataBindingUtil.setContentView<ActivityAddPointsBinding>(this, R.layout.activity_add_points)
        pointsMainBinding.point = ViewModelProviders.of(this, AddPointsViewModelFactory(this, this))
            .get(AddPointsViewModel::class.java)

        initializeView()
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        tvFullName = findViewById<TextView>(R.id.tvFullName)
        tvidUserPoint = findViewById<TextView>(R.id.tvidUserPoint)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnSuccess(message: String) {
        ProgresbarFinish()
        if(message != ""){
            if(message == "Save point"){
                Toasty.success(this, "Puntos agregados exitosamente", Toast.LENGTH_SHORT, true).show()
                val intent= Intent(applicationContext, PointsActivity::class.java)
                startActivity(intent)
                finish()
            }else{
                Toasty.success(this, message, Toast.LENGTH_SHORT, true).show()
            }
        }
    }

    override fun LoadUserByName(user: Users) {
        tvFullName!!.text = user.firstname + " " + user.surname
        tvidUserPoint!!.text = user.iduser.toString()
        ProgresbarFinish()
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        tvFullName!!.text = ""
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }

    override fun deletePoint(idpoint: String) {
    }
    //endregion View
}
