package java.app.lealtad.videogames.points.model

import androidx.databinding.BaseObservable

class Points : BaseObservable {

    var idpoint: Int = 0
    var iduser: Int = 0
    var points: Int = 0
    var totals: Int = 0
    lateinit var codefactura: String
    lateinit var description: String
    var iduseradd: Int = 0
    lateinit var dateadd: String
    var idusermod: Int = 0
    lateinit var datemod: String

    constructor() {}

    constructor(idpoint: Int, iduser: Int, points: Int, totals: Int, codefactura: String, description: String, iduseradd: Int, dateadd: String, idusermod: Int, datemod: String) {
        this.idpoint = idpoint
        this.iduser = iduser
        this.points = points
        this.totals = totals
        this.codefactura = codefactura
        this.description = description
        this.iduseradd = iduseradd
        this.dateadd = dateadd
        this.idusermod = idusermod
        this.datemod = datemod
    }
}