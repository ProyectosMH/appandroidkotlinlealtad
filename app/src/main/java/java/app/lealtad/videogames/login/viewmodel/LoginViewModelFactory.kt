package java.app.lealtad.videogames.login.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import java.app.lealtad.videogames.login.interfaces.EventsActivityCallback

class LoginViewModelFactory(private val loginActivityCallback: EventsActivityCallback, private  val context: Context) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return LoginViewModel(loginActivityCallback, context) as T
    }
}
