package java.app.lealtad.videogames.util

import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiUtil {
    private  val BaseUrl = "http://videosgames.cool/videoGameApi/api/"
    private var retrofit: Retrofit? = null

    val apiUtil: Retrofit?
        get() {
            if(retrofit == null){
                retrofit = Retrofit.Builder()
                    .baseUrl(BaseUrl).client(requestHeader)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }

            return retrofit
        }

    private val requestHeader: OkHttpClient
        get() = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build()
}