package java.app.lealtad.videogames.home.repository

import android.content.Context
import android.graphics.BitmapFactory
import android.util.Base64
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.app.lealtad.videogames.home.interfaces.HomeActivityCallback
import java.app.lealtad.videogames.home.interfaces.HomeServices
import java.app.lealtad.videogames.points.model.PointsResponse
import java.app.lealtad.videogames.home.model.Products
import java.app.lealtad.videogames.products.interfaces.ProductsServices
import java.app.lealtad.videogames.util.ApiUtilToken
import java.app.lealtad.videogames.util.BaseActivity

class HomeRepository(private val homeActivityCallback: HomeActivityCallback) : BaseActivity() {

    fun findProductbyName(nameProduct: String, puntos: String) {
        var service = ApiUtilToken.apiUtil?.create(ProductsServices::class.java)
        val call = service?.findProductbyName(nameProduct)

        call?.enqueue(object : Callback<List<Products>> {
            override fun onResponse(call: Call<List<Products>>, response: Response<List<Products>>) {
                if (response.code() == 200) {
                    var productsList: List<Products> = response.body()!!
                    var totalPuntosInt = Integer.parseInt(puntos)
                    productsList!!.forEach {
                        it.totalPuntosUser = puntos
                        if(it.image != null){
                            if(it.image != ""){
                                val decodedString = Base64.decode(it.image, Base64.NO_WRAP)
                                it.imagen = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                                var puntosInt = Integer.parseInt(it.points)
                                if(totalPuntosInt >= puntosInt){
                                    it.canjear =  true
                                }
                            }
                            if(it.promocion.equals("No")){
                                it.promo = true
                            }
                        }
                    }
                    homeActivityCallback.OnSuccessProducts(productsList, "")
                }else{
                    homeActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<List<Products>>, t: Throwable) {
                homeActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }

    fun getProductsMutableLiveData(points: String): MutableLiveData<List<Products>> {
        var liveData = MutableLiveData<List<Products>>()
        var service = ApiUtilToken.apiUtil?.create(HomeServices::class.java)
        val call = service?.getAllProducts("No")

        call?.enqueue(object : Callback<List<Products>> {
            override fun onResponse(call: Call<List<Products>>, response: Response<List<Products>>) {
                if (response.code() == 200) {
                    var totalPuntosInt = Integer.parseInt(points)
                    liveData.value = response.body()!!
                    liveData.value!!.forEach {
                        it.totalPuntosUser = points
                        if(it.image != null){
                            if(it.image != ""){
                                val decodedString = Base64.decode(it.image, Base64.NO_WRAP)
                                it.imagen = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                                var puntosInt = Integer.parseInt(it.points)
                                if(totalPuntosInt >= puntosInt){
                                    it.canjear =  true
                                }
                                //it.imageByte = Base64.decode(it.image, Base64.DEFAULT)
                                //it.image = ""
                            }
                            if(it.promocion.equals("No")){
                                it.promo = true
                            }
                        }
                    }
                    homeActivityCallback.OnSuccess("No")
                }else{
                    homeActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<List<Products>>, t: Throwable) {
                liveData.value = null
            }
        })

        return liveData
    }

    fun LoadPoints(context: Context): MutableLiveData<PointsResponse> {
        var liveData = MutableLiveData<PointsResponse>()
        var tokenUser: String = getValueSession(TOKEN_S, context, SESSION_PREFERENCES)
        val idUser = getValueSession(IDUSER_S, context, SESSION_PREFERENCES)
        var service = ApiUtilToken.apiUtil?.create(HomeServices::class.java)

        val call = service?.LoadPoints(idUser)

        call?.enqueue(object : Callback<PointsResponse> {
            override fun onResponse(call: Call<PointsResponse>, response: Response<PointsResponse>) {
                if (response.code() == 200) {
                    liveData.value = response.body()!!
                }else{
                    homeActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<PointsResponse>, t: Throwable) {
                liveData.value = null
            }
        })

        return liveData
    }

    fun findAllProducts(puntos: String, promo: String) {
        var service = ApiUtilToken.apiUtil?.create(HomeServices::class.java)
        val call = service?.getAllProducts(promo)

        call?.enqueue(object : Callback<List<Products>> {
            override fun onResponse(call: Call<List<Products>>, response: Response<List<Products>>) {
                if (response.code() == 200) {
                    var productsList: List<Products> = response.body()!!
                    homeActivityCallback.OnSuccessProducts(productsList, promo)

                    var totalPuntosInt = Integer.parseInt(puntos)
                    productsList!!.forEach {
                        it.totalPuntosUser = puntos
                        if(it.image != null){
                            if(it.image != ""){
                                val decodedString = Base64.decode(it.image, Base64.NO_WRAP)
                                it.imagen = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                                var puntosInt = Integer.parseInt(it.points)
                                if(totalPuntosInt >= puntosInt && !promo.equals("Si")){
                                    it.canjear =  true
                                }
                            }
                            if(it.promocion.equals("No")){
                                it.promo = true
                            }
                        }
                    }
                    homeActivityCallback.OnSuccess(promo)
                }else{
                    homeActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<List<Products>>, t: Throwable) {
                homeActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }
}