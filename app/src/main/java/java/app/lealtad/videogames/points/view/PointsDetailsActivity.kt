package java.app.lealtad.videogames.points.view

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.util.BaseActivity

class PointsDetailsActivity : BaseActivity() {

    var tvUserD: TextView? = null
    var tvPuntosD: TextView? = null
    var tvTotalD: TextView? = null
    var tvFacturaD: TextView? = null
    var tvDescripcionD: TextView? = null
    var tvUsuarioAddD: TextView? = null
    var tvFechaAddD: TextView? = null
    var ivhome: ImageView? = null
    var btnRegresar: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_points_details)
        title = "Detalle de los puntos"

        initializeView()

        btnRegresar!!.setOnClickListener {
            val intent= Intent(applicationContext, PointsActivity::class.java)
            startActivity(intent)
        }

        ivhome!!.setOnClickListener {
            val intent= Intent(applicationContext, HomeActivity::class.java)
            startActivity(intent)
        }
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        ivhome = findViewById<ImageView>(R.id.ivhome)
        tvUserD = findViewById<TextView>(R.id.tvUserD)
        tvPuntosD = findViewById<TextView>(R.id.tvPuntosD)
        tvTotalD = findViewById<TextView>(R.id.tvTotalD)
        tvFacturaD = findViewById<TextView>(R.id.tvFacturaD)
        tvDescripcionD = findViewById<TextView>(R.id.tvDescripcionD)
        tvUsuarioAddD = findViewById<TextView>(R.id.tvUsuarioAddD)
        tvFechaAddD = findViewById<TextView>(R.id.tvFechaAddD)
        btnRegresar = findViewById<Button>(R.id.btnRegresar)

        showDetail()
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun showDetail(){
        val intent = intent
        val user = intent.getStringExtra("USERNAME")
        val puntos = intent.getStringExtra("POINTS")
        val total = intent.getStringExtra("TOTALS")
        val factura = intent.getStringExtra("CODEFACTURA")
        val descripcion = intent.getStringExtra("DESCRIPCION")
        val asigno = intent.getStringExtra("USERADD")
        val fechaadd = intent.getStringExtra("DATEADD")

        if(user != "default value"){
            tvUserD!!.text = user
        }
        if(puntos != "default value"){
            tvPuntosD!!.text = puntos
        }
        if(total != "default value"){
            tvTotalD!!.text = total
        }
        if(factura != "default value"){
            tvFacturaD!!.text = factura
        }
        if(descripcion != "default value"){
            tvDescripcionD!!.text = descripcion
        }
        if(asigno != "default value"){
            tvUsuarioAddD!!.text = asigno
        }
        if(fechaadd != "default value"){
            tvFechaAddD!!.text = fechaadd
        }
    }
    //endregion View
}
