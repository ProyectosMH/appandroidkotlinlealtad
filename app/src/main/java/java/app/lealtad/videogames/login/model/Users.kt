package java.app.lealtad.videogames.login.model


import android.text.TextUtils
import androidx.databinding.BaseObservable

class Users : BaseObservable {
    var iduser: Int = 0
        get() = field
        set(userId) {
            field = userId
        }
    var firstname: String? = null
        get() = field
        set(firstNameU) {
            field = firstNameU
        }
    var secondname: String? = null
        get() = field
        set(secondNameU) {
            field = secondNameU
        }
    var surname: String? = null
        get() = field
        set(surnameU) {
            field = surnameU
        }
    var secondsurname: String? = null
        get() = field
        set(secondSurnameU) {
            field = secondSurnameU
        }
    var email: String? = null
        get() = field
        set(useremail) {
            field = useremail
        }
    var pass: String? = null
        get() = field
        set(userPassWord) {
            field = userPassWord
        }
    var phone: String? = null
        get() = field
        set(phoneUser) {
            field = phoneUser
        }
    var username: String? = null
        get() = field
        set(userName) {
            field = userName
        }
    var dateborn: String? = null
        get() = field
        set(dateBornU) {
            field = dateBornU
        }
    var iduseradd: Int = 0
        get() = field
        set(idUserAdd) {
            field = idUserAdd
        }
    var dateadd: String? = null
        get() = field
        set(dateAddU) {
            field = dateAddU
        }
    var idusermod: Int = 0
        get() = field
        set(idUserMod) {
            field = idUserMod
        }
    var datemod: String? = null
        get() = field
        set(dateModU) {
            field = dateModU
        }
    var idrol: Int = 0
        get() = field
        set(idRol) {
            field = idRol
        }

    val isCorrect: Boolean
        get() = !TextUtils.isEmpty(username) && !TextUtils.isEmpty(pass)

    constructor() {}

    constructor(iduser: Int, firstname: String, secondname: String, surname: String, secondsurname: String, email: String,
                pass: String, phone: String, username: String, dateborn: String, iduseradd: Int, dateadd: String, idusermod: Int,
                datemod: String, idrol: Int) {
        this.iduser = iduser
        this.firstname = firstname
        this.secondname = secondname
        this.surname = surname
        this.secondsurname = secondsurname
        this.email = email
        this.pass = pass
        this.phone = phone
        this.username = username
        this.dateborn = dateborn
        this.iduseradd = iduseradd
        this.dateadd = dateadd
        this.idusermod = idusermod
        this.datemod = datemod
        this.idrol = idrol
    }
}
