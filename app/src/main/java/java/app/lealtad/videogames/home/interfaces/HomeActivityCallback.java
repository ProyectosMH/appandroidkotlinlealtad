package java.app.lealtad.videogames.home.interfaces;

import java.app.lealtad.videogames.home.model.Products;
import java.util.List;

public interface HomeActivityCallback {
    void OnSuccess(String promo);
    void OnSuccessProducts(List<Products> productsList, String promo);
    void OnError(String message);
    void ProgresBarStar();
    void OnSuccessPoints(String points);
    void IsLoadList(String promo);
}
