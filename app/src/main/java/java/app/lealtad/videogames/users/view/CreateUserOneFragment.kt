package java.app.lealtad.videogames.users.view

import android.app.Activity
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText

import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.users.interfaces.UsersActivityCallback
import android.widget.Toast
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.login.model.Users

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CreateUserOneFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [CreateUserOneFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CreateUserOneFragment(user: Users) : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    lateinit var usersActivityCallback: UsersActivityCallback
    var user = user

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_create_user_one, container, false)

        val etPrimerNombre = view.findViewById<View>(R.id.etPrimerNombre) as EditText
        val etSegundoNombre = view.findViewById<View>(R.id.etSegundoNombre) as EditText
        val etPrimerApellido = view.findViewById<View>(R.id.etPrimerApellido) as EditText
        val etSegundoApellido = view.findViewById<View>(R.id.etSegundoApellido) as EditText
        val etEmail = view.findViewById<View>(R.id.etEmail) as EditText
        val etPhone = view.findViewById<View>(R.id.etPhone) as EditText
        val btnContinuar = view.findViewById<View>(R.id.btnContinuar) as Button

        if(user != null){
            if(user.firstname != null){
                etPrimerNombre?.setText(user.firstname)
            }
            if(user.secondname != null){
                etSegundoNombre?.setText(user.secondname)
            }
            if(user.firstname != null){
                etPrimerApellido?.setText(user.surname)
            }
            if(user.firstname != null){
                etSegundoApellido?.setText(user.secondsurname)
            }
            if(user.email != null){
                etEmail?.setText(user.email)
            }
            if(user.phone != null){
                etPhone?.setText(user.phone)
            }
        }

        btnContinuar.setOnClickListener {
            if(etPrimerNombre.text.toString() == ""){
                activity?.let { it1 -> Toasty.error(it1,"Campo primer nombre es requerido!", Toast.LENGTH_SHORT).show() }
            } else if(etPrimerApellido.text.toString() == ""){
                activity?.let { it1 -> Toasty.error(it1,"Campo primer apellido es requerido!", Toast.LENGTH_SHORT).show() }
            } else if(etEmail.text.toString() == ""){
                activity?.let { it1 -> Toasty.error(it1,"Campo email es requerido!", Toast.LENGTH_SHORT).show() }
            } else if(etPhone.text.toString() == ""){
                activity?.let { it1 -> Toasty.error(it1,"Campo telefono es requerido!", Toast.LENGTH_SHORT).show() }
            } else {
                if(isEmailValid(etEmail.text.toString())){
                    if(etPhone.text.toString().length == 8){
                        val number = etPhone.text.toString().substring(0, 1)
                        if(number == "7" || number == "6" || number == "2"){
                            val user: Users = Users(0, etPrimerNombre.text.toString(), etSegundoNombre.text.toString(), etPrimerApellido.text.toString(), etSegundoApellido.text.toString(), etEmail.text.toString(), "", etPhone.text.toString(), "", "", 0, "", 0, "", 0)

                            usersActivityCallback.ValidateFragmentOne(user)
                        }else{
                            activity?.let { it1 -> Toasty.error(it1,"Numero de telefono es incorrecto!", Toast.LENGTH_SHORT).show() }
                        }
                    } else{
                        activity?.let { it1 -> Toasty.error(it1,"Numero de telefono es incorrecto!", Toast.LENGTH_SHORT).show() }
                    }
                }else{
                    activity?.let { it1 -> Toasty.error(it1,"Email es incorrecto!", Toast.LENGTH_SHORT).show() }
                }
            }
        }

        return view
    }

    fun isEmailValid(email: CharSequence): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        try {
            usersActivityCallback = getActivity() as UsersActivityCallback
        } catch (e: ClassCastException) {
            throw ClassCastException("$activity must implement onButtonPressed")
        }

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }
}
