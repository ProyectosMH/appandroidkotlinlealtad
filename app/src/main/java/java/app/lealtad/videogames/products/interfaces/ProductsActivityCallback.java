package java.app.lealtad.videogames.products.interfaces;

import java.app.lealtad.videogames.home.model.Products;
import java.util.List;

public interface ProductsActivityCallback {
    void OnSuccess(String message);
    void OnSuccessProducts(List<Products> productsList);
    void OnError(String message);
    void ProgresBarStar();
    void deleteProduct(String idproduct);
    void isPromotion(boolean promotion);
}

