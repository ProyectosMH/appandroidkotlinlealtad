package java.app.lealtad.videogames.login.interfaces

import com.google.gson.JsonObject
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import java.app.lealtad.videogames.login.model.LoginResponse
import java.app.lealtad.videogames.login.model.ResponseObject
import java.app.lealtad.videogames.login.model.Users

interface LoginService {
    @POST("Login")
    fun getLoginUser(@Body user: RequestBody): Call<JsonObject>

    @POST("authentification/login")
    fun getTokenValidate(@Header("username") username: String, @Header("password") password: String): Call<LoginResponse>

    @POST("authentification/saveUsers")
    fun saveUsers(@Body user: RequestBody): Call<ResponseObject>

    @POST("authentification/passForget")
    fun passForget(@Header("username") username: String, @Header("email") email: String, @Header("phone") phone: String, @Header("password") password: String): Call<ResponseObject>
}