package java.app.lealtad.videogames.users.repository

import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.collection.ArrayMap
import androidx.lifecycle.MutableLiveData
import es.dmoral.toasty.Toasty
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.login.interfaces.LoginService
import java.app.lealtad.videogames.login.model.ResponseObject
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.login.view.MainActivity
import java.app.lealtad.videogames.users.interfaces.UsersActivityCallback
import java.app.lealtad.videogames.users.interfaces.UsersServices
import java.app.lealtad.videogames.users.model.UsersResponse
import java.app.lealtad.videogames.users.view.UsersActivity
import java.app.lealtad.videogames.util.ApiUtil
import java.app.lealtad.videogames.util.ApiUtilToken
import java.app.lealtad.videogames.util.BaseActivity

class UsersRepository(private val usersActivityCallback: UsersActivityCallback, private val context: Context) : BaseActivity() {

    fun getUsersMutableLiveData(): MutableLiveData<List<UsersResponse>> {
        var liveData = MutableLiveData<List<UsersResponse>>()
        var service = ApiUtilToken.apiUtil?.create(UsersServices::class.java)
        val call = service?.getUsersList()

        call?.enqueue(object : Callback<List<UsersResponse>> {
            override fun onResponse(call: Call<List<UsersResponse>>, response: Response<List<UsersResponse>>) {
                if (response.code() == 200) {
                    liveData.value = response.body()!!
                }else{
                    usersActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<List<UsersResponse>>, t: Throwable) {
                liveData.value = null
            }
        })

        return liveData
    }

    fun goHomeLogin(){
        var idUser = getValueSession(IDUSER_S, context, SESSION_PREFERENCES)
        if(idUser != "default value"){
            val intent= Intent(context, HomeActivity::class.java)
            context?.startActivity(intent)
        }else{
            val intent= Intent(context, MainActivity::class.java)
            context?.startActivity(intent)
        }
    }

    fun findProductbyName(nameProduct: String) {
        var service = ApiUtilToken.apiUtil?.create(UsersServices::class.java)
        val call = service?.findUserByNameList(nameProduct)

        call?.enqueue(object : Callback<List<UsersResponse>> {
            override fun onResponse(call: Call<List<UsersResponse>>, response: Response<List<UsersResponse>>) {
                if (response.code() == 200) {
                    var usersList: List<UsersResponse> = response.body()!!
                    if(usersList.isNotEmpty()){
                        usersActivityCallback.OnSuccessUsers(usersList)
                    }else{
                        usersActivityCallback.OnError("No se encontro ningun usuario")
                    }
                }else{
                    usersActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<List<UsersResponse>>, t: Throwable) {
                usersActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }

    fun saveUSer(user: Users) {
        var idUser = getValueSession(IDUSER_S, context, SESSION_PREFERENCES)
        if(idUser == "default value"){
            idUser = "0"
        }
        val jsonParams = ArrayMap<String, Any>()
        jsonParams.put("iduser", user.iduser)
        jsonParams.put("firstname", user.firstname)
        jsonParams.put("secondname", user.secondname)
        jsonParams.put("surname", user.surname)
        jsonParams.put("secondsurname", user.secondsurname)
        jsonParams.put("email", user.email)
        jsonParams.put("pass", user.pass)
        jsonParams.put("phone", user.phone)
        jsonParams.put("username", user.username)
        jsonParams.put("dateborn", user.dateborn)
        jsonParams.put("iduseradd", idUser)
        jsonParams.put("dateadd", user.dateadd)
        jsonParams.put("idusermod", user.idusermod)
        jsonParams.put("datemod", user.datemod)
        jsonParams.put("idrol", user.idrol)

        val body = RequestBody.create(
            okhttp3.MediaType.parse("application/json; charset=utf-8"),
            JSONObject(jsonParams).toString()
        )


        if(idUser == "0"){
            var service = ApiUtil.apiUtil?.create(LoginService::class.java)
            val call = service?.saveUsers(body)
            var responseObject: ResponseObject

            call?.enqueue(object : Callback<ResponseObject> {
                override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                    if (response.code() == 200) {
                        responseObject = response.body()!!
                        if(responseObject.success){
                            if(idUser == "0"){
                                setValueSession(LOGOUT_S, "logout", context, LOGOUT_PREFERENCES)
                                Toasty.success(context, "Usuario creado con exito, ingresa con tus credenciales", Toast.LENGTH_LONG, true).show()
                                val intent= Intent(context, MainActivity::class.java)
                                context?.startActivity(intent)
                            }else{
                                usersActivityCallback.OnSuccess("User Save")
                            }
                        }else {
                            usersActivityCallback.OnError(responseObject.message)
                        }
                    }else {
                        usersActivityCallback.OnError("Problemas en la coneccion con el servidor")
                    }
                }
                override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                    usersActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            })
        }else{
            var service = ApiUtilToken.apiUtil?.create(UsersServices::class.java)
            val call = service?.saveUsers(body)
            var responseObject: ResponseObject

            call?.enqueue(object : Callback<ResponseObject> {
                override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                    if (response.code() == 200) {
                        responseObject = response.body()!!
                        if(responseObject.success){
                            if(idUser == "0"){
                                Toasty.success(context, "Usuario creado con exito, ingresa con tus credenciales", Toast.LENGTH_SHORT, true).show()
                                val intent= Intent(context, MainActivity::class.java)
                                context?.startActivity(intent)
                            }else{
                                usersActivityCallback.OnSuccess("User Save")
                            }
                        }else {
                            usersActivityCallback.OnError(responseObject.message)
                        }
                    }else {
                        usersActivityCallback.OnError("Problemas en la coneccion con el servidor")
                    }
                }
                override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                    usersActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            })
        }

    }

    fun deleteUser(iduser: String){
        var service = ApiUtilToken.apiUtil?.create(UsersServices::class.java)
        val call = service?.deleteUser(iduser)
        var responseObject: ResponseObject

        call?.enqueue(object : Callback<ResponseObject> {
            override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                if (response.code() == 200) {
                    responseObject = response.body()!!
                    val intent = Intent(context, UsersActivity::class.java)
                    context?.startActivity(intent)
                    finish()
                    usersActivityCallback.OnSuccess(responseObject.message)
                }else {
                    usersActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                usersActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }

    fun changePass(passActual: String, newPass: String){
        val userName = getValueSession(USER_S, context, SESSION_PREFERENCES)
        var service = ApiUtilToken.apiUtil?.create(UsersServices::class.java)
        val call = service?.changePass(userName, passActual, newPass)
        var responseObject: ResponseObject

        call?.enqueue(object : Callback<ResponseObject> {
            override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
                if (response.code() == 200) {
                    responseObject = response.body()!!
                    if(responseObject.message == "Contraseña actualizada exitosamente"){
                        usersActivityCallback.OnSuccess(responseObject.message)
                    }else{
                        usersActivityCallback.OnError(responseObject.message)
                    }
                }else {
                    usersActivityCallback.OnError("Problemas en la coneccion con el servidor")
                }
            }
            override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                usersActivityCallback.OnError("Problemas en la coneccion con el servidor")
            }
        })
    }
}