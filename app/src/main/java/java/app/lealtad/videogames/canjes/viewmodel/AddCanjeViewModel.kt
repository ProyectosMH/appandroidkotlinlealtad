package java.app.lealtad.videogames.canjes.viewmodel

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.canjes.interfaces.CanjesActivityCallback
import java.app.lealtad.videogames.canjes.model.Canjes
import java.app.lealtad.videogames.canjes.repository.CanjesRepository
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.points.model.PointsResponse
import java.app.lealtad.videogames.points.interfaces.PointsActivityCallback
import java.app.lealtad.videogames.points.model.Points
import java.app.lealtad.videogames.points.repository.PointsRepository
import java.app.lealtad.videogames.points.view.AddPointsActivity

class AddCanjeViewModel: ViewModel {

    var canjesActivityCallback: CanjesActivityCallback
    var context: Context
    var canjesRepo: CanjesRepository
    var userName: String = ""
    var productName: String = ""
    var puntosUser: String = ""
    var puntosProduct: String = ""
    var idUser: String = ""
    var idProduct: String = ""

    fun goHome(view: View){
        val intent= Intent(context, HomeActivity::class.java)
        context?.startActivity(intent)
    }

    fun setvalues(idUser: String, userName: String, idProduct: String, productName: String, puntosProduct: String, puntosUser: String){
        this.idUser = idUser
        this.userName = userName
        this.idProduct = idProduct
        this.productName = productName
        this.puntosProduct = puntosProduct
        this.puntosUser = puntosUser
    }

    fun saveCanje(view: View){
        if(idUser == "" || userName == "" || idProduct == "" || productName == "" || puntosUser == "" || puntosProduct == ""){
            Toasty.error(context, "Canje no puede ser procesado porque falta informacion en el QR", Toast.LENGTH_SHORT, true).show()
        } else {
            canjesActivityCallback.ProgresBarStar()
            val canje = Canjes("0", idUser, idProduct, productName, puntosProduct, puntosUser, "0", "0", "", "", "", "", "")
            canjesRepo.saveCanje(canje, context)
        }
    }

    constructor(canjesActivityCallback: CanjesActivityCallback, context: Context) {
        this.canjesActivityCallback = canjesActivityCallback
        this.context = context
        canjesRepo = CanjesRepository(canjesActivityCallback)
    }
}