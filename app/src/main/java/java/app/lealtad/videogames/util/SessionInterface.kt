package java.app.lealtad.videogames.util

interface SessionInterface {
    fun onSessionLogout()
}