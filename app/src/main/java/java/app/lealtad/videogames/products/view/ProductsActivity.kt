package java.app.lealtad.videogames.products.view

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.databinding.ActivityProductsBinding
import java.app.lealtad.videogames.home.adapter.HomeAdapter
import java.app.lealtad.videogames.home.adapter.ProductsAdapter
import java.app.lealtad.videogames.home.model.Products
import java.app.lealtad.videogames.products.interfaces.ProductsActivityCallback
import java.app.lealtad.videogames.products.viewmodel.ProductsViewModel
import java.app.lealtad.videogames.products.viewmodel.ProductsViewModelFactory
import java.app.lealtad.videogames.util.BaseActivity

class ProductsActivity : BaseActivity(), ProductsActivityCallback  {

    var productsViewModel: ProductsViewModel? = null
    var productsAdapter: ProductsAdapter? = null
    var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_products)
        title = "Productos"

        val productsMainBinding = DataBindingUtil.setContentView<ActivityProductsBinding>(this, R.layout.activity_products)

        productsMainBinding.products = ViewModelProviders.of(this, ProductsViewModelFactory(this, this))
            .get(ProductsViewModel::class.java)

        productsViewModel = ViewModelProviders.of(this, ProductsViewModelFactory(this, this)).get(ProductsViewModel::class.java)

        initializeView()

        recyclerView = findViewById<RecyclerView>(R.id.recyclerProducts)
        productsViewModel!!.getDataProducts().observe(this,
            Observer<List<Products>> { arrayList ->
                productsAdapter = ProductsAdapter(this@ProductsActivity, arrayList, this, "", "")

                recyclerView!!.layoutManager = LinearLayoutManager(this@ProductsActivity)
                recyclerView!!.adapter = productsAdapter
            })
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnSuccess(message: String) {
        ProgresbarFinish()
        if(!message.equals("")){
            Toasty.success(this, message, Toast.LENGTH_SHORT, true).show()
        }
    }

    override fun OnSuccessProducts(productsList: List<Products>) {
        ProgresbarFinish()
        if(productsList != null){

            productsAdapter = ProductsAdapter(this@ProductsActivity, productsList, this, "", "")

            recyclerView!!.layoutManager = LinearLayoutManager(this@ProductsActivity)
            recyclerView!!.adapter = productsAdapter
        }
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }

    override fun deleteProduct(idproduct: String) {
        productsViewModel!!.deleteProduct(idproduct)
    }

    override fun isPromotion(promotion: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    //endregion View
}
