package java.app.lealtad.videogames.util

import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import com.google.android.material.navigation.NavigationView
import java.app.lealtad.videogames.R

import androidx.drawerlayout.widget.DrawerLayout
import android.widget.TextView
import android.app.KeyguardManager
import android.view.Menu
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import android.content.Intent
import android.content.SharedPreferences
import java.app.lealtad.videogames.login.view.MainActivity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.Nullable
import es.dmoral.toasty.Toasty

import androidx.biometric.BiometricPrompt
import androidx.fragment.app.FragmentActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.app.lealtad.videogames.canjes.view.CanjesActivity
import java.app.lealtad.videogames.geolocalizacion.view.MapsActivity
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.login.interfaces.LoginService
import java.app.lealtad.videogames.login.model.LoginResponse
import java.app.lealtad.videogames.points.view.PointsActivity
import java.app.lealtad.videogames.products.view.ProductsActivity
import java.app.lealtad.videogames.users.view.ChangePassActivity
import java.app.lealtad.videogames.users.view.UsersActivity
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.concurrent.Executors
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

open class BaseActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, SessionInterface {

    val SESSION_PREFERENCES = "Session"
    val USER_S = "UserS"
    val PASS_S = "PasS"
    val IDUSER_S = "IdUserS"
    val ROLUSER_S = "RolUserS"
    private val USER_FINGER = "USUARIO_F"
    private val PASS_FINGER = "PASS_F"
    val TOKEN_S = "TokenS"
    val DATEBORN_S = "FechaCumpleS"
    val LOGOUT_S = "cerrar_Ses_Login"
    val LOGOUT_PREFERENCES = "logout_session"
    val FINGER_PRINT_PREFERENCES = "FingerPrintAuth_Prefe"
    val CUMPLE_PREFERENCES = "CumplePrefeS"
    val LOGIN_PP = "Session_terminada"
    private var pausedMillis: Long = Calendar.getInstance().getTimeInMillis()
    public final var timeLogout: Long = 1000 * 60 * 5

    val TIPOENC: String = "AES"
    val MODO_ENC: String = "AES/CBC/PKCS5Padding"
    val IV_ENC: String = "1q2w3e4r5t6y7u8i"
    val KEY_ENC: String = "0p9o8i7u6y5t4r3e"

    lateinit var llProgressBar: LinearLayout
    private var fingerExist = false

    lateinit var drawerLayout: DrawerLayout
    lateinit var toolbar: Toolbar
    lateinit var toggle: ActionBarDrawerToggle
    lateinit var navigationView: NavigationView
    lateinit var menu: Menu

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (this !is MainActivity) {
            (application as VideoGamesApp).registerUserSessionListener(this)
            (application as VideoGamesApp).starUserSession()
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()

        if (this !is MainActivity) {
            (application as VideoGamesApp).onUserInteraction()
        }
    }

    override fun onSessionLogout() {
        setValueSession(LOGIN_PP, "sesion_expire_pp", applicationContext, SESSION_PREFERENCES)
    }

    override fun onStop() {
        super.onStop()

        pausedMillis = Calendar.getInstance().getTimeInMillis()
    }

    override fun onResume() {
        super.onResume()

        try {
            val lg_pp = getValueSession(LOGIN_PP, applicationContext, SESSION_PREFERENCES)
            val actividad = this.javaClass.simpleName

            if (actividad != "MainActivity" && lg_pp != null && lg_pp == "sesion_expire_pp") {
                val sharedpreferences =
                    getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE)
                val editor = sharedpreferences.edit()
                editor.clear()
                editor.commit()

                val login = Intent(this, MainActivity::class.java)
                setValueSession(LOGOUT_S, "logout", applicationContext, LOGOUT_PREFERENCES)
                login.putExtra("SESION", "Tu sesión ha expirado")
                login.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivityForResult(login, 0)
                finish()
            }

            val currentMillis = Calendar.getInstance().getTimeInMillis()
            if (this !is MainActivity && currentMillis - pausedMillis > timeLogout) {
                val sharedpreferences =
                    getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE)
                val editor = sharedpreferences.edit()
                editor.clear()
                editor.commit()

                val login = Intent(this, MainActivity::class.java)
                setValueSession(LOGOUT_S, "logout", applicationContext, LOGOUT_PREFERENCES)
                login.putExtra("SESION", "Tu sesión ha expirado")
                login.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivityForResult(login, 0)
                finish()
            }
        } catch (e: Exception) {

        }
    }

    fun menu() {
        navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        navigationView.itemIconTintList = null

        val header = navigationView.getHeaderView(0)
        val tvUser = header.findViewById<TextView>(R.id.tvUser)
        val userSession = getValueSession(USER_S, applicationContext, SESSION_PREFERENCES)
        val rolId: String = getValueSession(ROLUSER_S, applicationContext, SESSION_PREFERENCES)

        if (!userSession.equals("default value")) {
            tvUser.text = userSession
        }

        if (verifyFingerDevice()) {
            menu = navigationView.menu
            val userFinger = getValueSession(USER_FINGER, applicationContext, FINGER_PRINT_PREFERENCES)

            if (!userFinger.equals("default value")) {
                menu.findItem(R.id.iActivarHuella).isVisible = false
            } else {
                menu.findItem(R.id.iDesactivarHuella).isVisible = false
            }
        } else {
            menu = navigationView.menu
            menu.findItem(R.id.iActivarHuella).isVisible = false
            menu.findItem(R.id.iDesactivarHuella).isVisible = false
        }

        if(!rolId.equals("1")){
            menu = navigationView.menu
            menu.findItem(R.id.iAddUSer).isVisible = false
            menu.findItem(R.id.iAddProducts).isVisible = false
        }

        toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(findViewById<Toolbar>(R.id.toolbar))
        drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        if(rolId != "default value") {
            toggle = ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
            )
            drawerLayout.addDrawerListener(toggle)
            toggle.syncState()
        }

        val mTitle = toolbar.findViewById<TextView>(R.id.toolbar_title)
        if(mTitle != null){
            mTitle.text = toolbar.title
        }
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.iLogout) {
            logout()
        } else if (id == R.id.iActivarHuella) {
            showBiometricPrompt()
        } else if (id == R.id.iDesactivarHuella) {
            disabledFinger()
        } else if (id == R.id.iGigtPoint) {
            viewLoad("points")
        } else if (id == R.id.iGigtCanjes) {
            viewLoad("canjes")
        } else if (id == R.id.iAddProducts) {
            viewLoad("addProducts")
        } else if (id == R.id.iAddUSer) {
            viewLoad("addUsers")
        } else if (id == R.id.iGeolocalizacion) {
            viewLoad("geolocalizacion")
        } else if(id == R.id.iChangePass){
            viewLoad("changePass")
        } else {
            Toasty.error(applicationContext, "Menu sin funcionabilidad", Toast.LENGTH_SHORT, true).show()
        }

        drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    fun viewLoad(actividad: String){
        if (actividad.equals("points")) {
            val pointView = Intent(this, PointsActivity::class.java)
            startActivityForResult(pointView, 0)
            finish()
        } else if (actividad.equals("canjes")) {
            val canjesView = Intent(this, CanjesActivity::class.java)
            startActivityForResult(canjesView, 0)
            finish()
        } else if (actividad.equals("geolocalizacion")) {
            val mapsView = Intent(this, MapsActivity::class.java)
            startActivityForResult(mapsView, 0)
            finish()
        } else if (actividad.equals("addUsers")) {
            val usersView = Intent(this, UsersActivity::class.java)
            startActivityForResult(usersView, 0)
            finish()
        } else if (actividad.equals("addProducts")) {
            val productsView = Intent(this, ProductsActivity::class.java)
            startActivityForResult(productsView, 0)
            finish()
        } else if (actividad.equals("changePass")) {
            val changePassView = Intent(this, ChangePassActivity::class.java)
            startActivityForResult(changePassView, 0)
            finish()
        }
    }

    fun logout() {
        val sharedpreferences = getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE)
        var editor = sharedpreferences.edit()
        editor.clear()
        editor.apply()

        setValueSession(LOGOUT_S, "logout", applicationContext, LOGOUT_PREFERENCES)

        val home = Intent(this, MainActivity::class.java)
        startActivityForResult(home, 0)
        finish()
    }

    fun deleteSession(){
        val sharedpreferences: SharedPreferences = getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedpreferences.edit()
        editor.clear()
        editor.commit()
    }

    fun deleteLogout(){
        val sharedpreferences: SharedPreferences = getSharedPreferences(LOGOUT_PREFERENCES, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedpreferences.edit()
        editor.clear()
        editor.commit()
    }

    fun deleteCumple(){
        val sharedpreferences: SharedPreferences = getSharedPreferences(CUMPLE_PREFERENCES, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedpreferences.edit()
        editor.clear()
        editor.commit()
    }

    fun encriptar(value: String): String {
        try {
            val encrpt = value.toByteArray(charset("UTF-8"))
            return Base64.encodeToString(encrpt, Base64.NO_WRAP)
        } catch (e: Exception) {
            return "error"
        }

    }

    fun desencriptar(value: String): String{
        try{
            val data = Base64.decode(value, Base64.NO_WRAP)
            val text: String = String(data, StandardCharsets.UTF_8)
            return text
        }catch(e: Exception){
            return e.message!!
        }
    }

    fun encriptarKVA(value: String): String{
        try{
            val secretKeySpec: SecretKeySpec = SecretKeySpec(KEY_ENC.toByteArray(), TIPOENC)
            val cipher: Cipher = Cipher.getInstance(MODO_ENC)
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, IvParameterSpec(IV_ENC.toByteArray()))
            val values = cipher.doFinal(value.toByteArray())
            return Base64.encodeToString(values, Base64.DEFAULT)
        }catch (e: Exception){
            return "Tu dispositivo no soporta el tipo de encriptacion usado";
        }
    }

    fun desencriptarKVA(value: String): String{
        try{
            val values = Base64.decode(value, Base64.DEFAULT)
            val secretKeySpec: SecretKeySpec = SecretKeySpec(KEY_ENC.toByteArray(), TIPOENC)
            val cipher: Cipher = Cipher.getInstance(MODO_ENC)
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, IvParameterSpec(IV_ENC.toByteArray()))
            return String(cipher.doFinal(values))
        }catch(e: Exception){
            return  e.message!!
        }
    }

    fun setValueSession(key: String, value: String, context: Context, tipoPrefe: String) {
        val sharedPreferences = getSharedPreferences(tipoPrefe, context)
        var edit = sharedPreferences.edit()
        edit.putString(key, value)
        edit.commit()
    }

    fun getValueSession(key: String, context: Context, preferencesType: String): String {
        val sharedPreferences = getSharedPreferences(preferencesType, context)
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, "default value")
        }
        return "default value"
    }

    fun getSharedPreferences(preferencesType: String, context: Context): SharedPreferences {
        return context.getSharedPreferences(preferencesType, 0)
    }

    fun verifyFingerDevice(): Boolean {
        fingerExist = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT < 29) {
                val keyguardManager : KeyguardManager = applicationContext.getSystemService(KEYGUARD_SERVICE) as KeyguardManager
                val packageManager : PackageManager = applicationContext.packageManager
                if(!packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
                    fingerExist = false
                }
                if (!keyguardManager.isKeyguardSecure) {
                    fingerExist = false
                }
            }
        }else{
            fingerExist = false
        }
        return fingerExist
    }

    private fun showBiometricPrompt() {
        val executor = Executors.newSingleThreadExecutor()
        val activity: FragmentActivity = this
        val biometricPrompt = BiometricPrompt(activity, executor, object : BiometricPrompt.AuthenticationCallback() {

            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                    // user clicked negative button
                } else {

                }
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                loginFinger()
            }

        })

        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Autenticación Biometrica")
            .setSubtitle("Usa tu credencial biometrica")
            .setNegativeButtonText("Cancelar")
            .build()

        biometricPrompt.authenticate(promptInfo)
    }

    fun fingerEnabled(){
        val fingerUser = getValueSession(USER_FINGER, applicationContext, FINGER_PRINT_PREFERENCES)
        if(!fingerUser.equals("default value")){
            if(verifyFingerDevice()){
                showBiometricPrompt()
            }
        }
    }

    fun loginFinger(){
        val fingerUser = getValueSession(USER_FINGER, applicationContext, FINGER_PRINT_PREFERENCES)

        if(!fingerUser.equals("default value")){
            llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)

            runOnUiThread(
                object : Runnable {
                    override fun run() {
                        llProgressBar.visibility = View.VISIBLE
                    }
                }
            )

            var user = getValueSession(USER_FINGER, applicationContext, FINGER_PRINT_PREFERENCES)
            user = encriptar(user.toString())
            val pass = getValueSession(PASS_FINGER, applicationContext, FINGER_PRINT_PREFERENCES)
            var service = ApiUtil.apiUtil?.create(LoginService::class.java)
            val call = service?.getTokenValidate(user.toString(), pass.toString())
            var userResponse: LoginResponse

            call?.enqueue(object : Callback<LoginResponse> {
                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                    if (response.code() == 200) {
                        userResponse = response.body()!!
                        if(userResponse.username.equals("Usuario o contraseña incorrecta")){
                            Toasty.error(applicationContext, "Usuario o contraseña incorrecta", Toast.LENGTH_SHORT, true).show()
                        }else{
                            setValueSession(USER_S, userResponse.username.toString(), applicationContext, SESSION_PREFERENCES)
                            setValueSession(PASS_S, pass, applicationContext, SESSION_PREFERENCES)
                            setValueSession(IDUSER_S, userResponse.iduser.toString(), applicationContext, SESSION_PREFERENCES)
                            setValueSession(ROLUSER_S, userResponse.idrol .toString(), applicationContext, SESSION_PREFERENCES)
                            setValueSession(DATEBORN_S, userResponse.dateborn.toString(), applicationContext, SESSION_PREFERENCES)
                            setValueSession(DATEBORN_S, userResponse.dateborn.toString(), applicationContext, CUMPLE_PREFERENCES)
                            setValueSession(TOKEN_S, userResponse.token.toString(), applicationContext, SESSION_PREFERENCES)
                            runOnUiThread(
                                object : Runnable {
                                    override fun run() {
                                        llProgressBar.visibility = View.GONE
                                    }
                                })
                            homePage()
                        }
                    }
                }
                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    runOnUiThread(
                        object : Runnable {
                            override fun run() {
                                llProgressBar.visibility = View.GONE
                            }
                        })
                    Toasty.error(applicationContext, "Problemas en la coneccion con el servidor", Toast.LENGTH_SHORT, true).show()
                }
            })
        }else{
            val user = getValueSession(USER_S, applicationContext, SESSION_PREFERENCES)
            val pass = getValueSession(PASS_S, applicationContext, SESSION_PREFERENCES)
            setValueSession(USER_FINGER, user.toString(), applicationContext, FINGER_PRINT_PREFERENCES)
            setValueSession(PASS_FINGER, pass.toString(), applicationContext, FINGER_PRINT_PREFERENCES)
            finish()
            startActivity(intent)
        }
    }

    fun ProgresbarStar() {
        llProgressBar.visibility = View.VISIBLE
    }

    fun ProgresbarFinish() {
        llProgressBar.visibility = View.GONE
    }

    fun disabledFinger(){
        val sharedpreferences = getSharedPreferences(FINGER_PRINT_PREFERENCES, applicationContext)
        var editor = sharedpreferences.edit()
        editor.clear()
        editor.commit()
        /*finish()
        startActivity(intent)*/
        menu.findItem(R.id.iActivarHuella).isVisible = true
        menu.findItem(R.id.iDesactivarHuella).isVisible = false
        Toasty.success(applicationContext, "Huella desactivada exitosamente", Toast.LENGTH_SHORT).show()
    }

    fun getToken(): String {
        val context = VideoGamesApp.getContext()
        return getValueSession(TOKEN_S, context, SESSION_PREFERENCES)
    }

    fun homePage(){
        val home = Intent(this, HomeActivity::class.java)
        startActivityForResult(home, 0)
        finish()
    }
}
