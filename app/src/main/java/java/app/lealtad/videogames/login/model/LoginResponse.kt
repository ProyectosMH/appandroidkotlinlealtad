package java.app.lealtad.videogames.login.model

import androidx.databinding.BaseObservable

public class LoginResponse : BaseObservable {
    var iduser: Int = 0
        get() = field
        set(idUser) {
            field = idUser
        }
    var idrol: Int = 0
        get() = field
        set(idRol) {
            field = idRol
        }
    var firstname: String? = null
        get() = field
        set(firstName) {
            field = firstName
        }
    var username: String? = null
        get() = field
        set(userName) {
            field = userName
        }

    var dateborn: String? = null
        get() = field
        set(dateBorn) {
            field = dateBorn
        }
    var token: String? = null
        get() = field
        set(tokenU) {
            field = tokenU
        }

    constructor() {}

    constructor(iduser: Int, idrol: Int, firstname: String, username: String, dateborn: String, token: String) {
        this.iduser = iduser
        this.idrol = idrol
        this.firstname = firstname
        this.username = username
        this.dateborn = dateborn
        this.token = token
    }
}
