package java.app.lealtad.videogames.products.view

import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.home.model.Products
import java.app.lealtad.videogames.home.view.HomeActivity
import java.app.lealtad.videogames.util.BaseActivity
import java.util.ArrayList

class ProductDetailsActivity : BaseActivity() {

    var ivImageProduct: ImageView? = null
    var ivhome: ImageView? = null
    var tvNameProduct: TextView? = null
    var tvDescription: TextView? = null
    var tvPrice: TextView? = null
    var tvPoints: TextView? = null
    var btnRegresar: Button? = null
    var actividad: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_product_details)
        title = "Detalle del producto"

        initializeView()

        btnRegresar!!.setOnClickListener {
            if (actividad == "HomeActivity") {
                val intent= Intent(applicationContext, HomeActivity::class.java)
                startActivity(intent)
            } else {
                val intent= Intent(applicationContext, ProductsActivity::class.java)
                startActivity(intent)
            }
        }

        ivhome!!.setOnClickListener {
            val intent= Intent(applicationContext, HomeActivity::class.java)
            startActivity(intent)
        }
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        ivImageProduct = findViewById<ImageView>(R.id.ivImageProduct)
        ivhome = findViewById<ImageView>(R.id.ivhome)
        tvNameProduct = findViewById<TextView>(R.id.tvNameProduct)
        tvDescription = findViewById<TextView>(R.id.tvDescription)
        tvPrice = findViewById<TextView>(R.id.tvPrice)
        tvPoints = findViewById<TextView>(R.id.tvPoints)
        btnRegresar = findViewById<Button>(R.id.btnRegresar)

        showDetail()
    }
    //endregion Iniciar componentes

    //region View
    /*override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }*/

    fun showDetail(){
        val intent = intent
        val productName = intent.getStringExtra("PRODUCTNAME")
        val description = intent.getStringExtra("DESCRIPTION")
        val imageBase = intent.getStringExtra("IMAGEBASE")
        val price = intent.getStringExtra("PRICE")
        val points = intent.getStringExtra("POINTS")
        val promo = intent.getStringExtra("PROMOTION")

        /*val extras = intent.extras
        extras.apply {
            //Serializable Data
            val blog = getSerializable("productList") as List<Products>?
            if (blog != null) {
            }
        }*/

        actividad = intent.getStringExtra("ACTIVITY")
        if(productName != "default value"){
            tvNameProduct!!.text = productName
        }
        if(description != "default value"){
            tvDescription!!.text = description
        }
        if(imageBase != "default value"){
            /*val decodedString = Base64.decode(imageBase, Base64.NO_WRAP)
            var imageBitmap: Bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)*/
            val imageBitmap = BitmapFactory.decodeFile(imageBase)
            ivImageProduct!!.setImageBitmap(imageBitmap)
        }
        if(price != "default value"){
            tvPrice!!.text = "$" + price
        }
        if(points != "default value"){
            tvPoints!!.text = points + " Pts"
        }
        if(promo.equals("Si")){
            tvPoints!!.visibility = View.GONE
        }else{
            tvPoints!!.visibility = View.VISIBLE
        }
    }
    //endregion View
}
