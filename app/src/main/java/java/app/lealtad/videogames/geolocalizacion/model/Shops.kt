package java.app.lealtad.videogames.geolocalizacion.model

import androidx.databinding.BaseObservable

public class Shops : BaseObservable {

    lateinit var nombre: String
    lateinit var latitude: String
    lateinit var longitude: String
    lateinit var description: String

    constructor() {}

    constructor(nombre: String, latitude: String, longitude: String, description: String) {
        this.nombre = nombre
        this.latitude = latitude
        this.longitude = longitude
        this.description = description
        this.description = description
    }
}
