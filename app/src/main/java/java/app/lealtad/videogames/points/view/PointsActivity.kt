package java.app.lealtad.videogames.points.view

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import es.dmoral.toasty.Toasty
import java.app.lealtad.videogames.R
import java.app.lealtad.videogames.databinding.ActivityPointsBinding
import java.app.lealtad.videogames.login.model.Users
import java.app.lealtad.videogames.points.model.PointsResponse
import java.app.lealtad.videogames.points.adapter.PointsAdapter
import java.app.lealtad.videogames.points.interfaces.PointsActivityCallback
import java.app.lealtad.videogames.points.viewmodel.PointsViewModel
import java.app.lealtad.videogames.points.viewmodel.PointsViewModelFactory
import java.app.lealtad.videogames.util.BaseActivity

class PointsActivity : BaseActivity(), PointsActivityCallback {

    var pointsViewModel: PointsViewModel? = null
    var pointsAdapter: PointsAdapter? = null
    var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_points)
        title = "Puntos"

        val pointsMainBinding = DataBindingUtil.setContentView<ActivityPointsBinding>(this, R.layout.activity_points)
        pointsMainBinding.points = ViewModelProviders.of(this, PointsViewModelFactory(this, this))
            .get(PointsViewModel::class.java)

        pointsViewModel = ViewModelProviders.of(this, PointsViewModelFactory(this, this)).get(PointsViewModel::class.java)

        initializeView()

        recyclerView = findViewById<RecyclerView>(R.id.recyclerPoints)
        pointsViewModel!!.getDataPoints().observe(this,
            Observer<List<PointsResponse>> { arrayList ->
                pointsAdapter = PointsAdapter(this@PointsActivity, arrayList, this)

                recyclerView!!.layoutManager = LinearLayoutManager(this@PointsActivity)
                recyclerView!!.adapter = pointsAdapter
            })
    }

    //region Iniciar componentes
    fun initializeView(){
        menu()

        llProgressBar = findViewById<LinearLayout>(R.id.llProgressBar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
    //endregion Iniciar componentes

    //region View
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnSuccess(message: String) {
        ProgresbarFinish()
        if(message != ""){
            Toasty.success(this, message, Toast.LENGTH_SHORT, true).show()
        }
    }

    override fun LoadUserByName(user: Users) {
    }

    override fun OnError(message: String) {
        ProgresbarFinish()
        Toasty.error(this, message, Toast.LENGTH_SHORT, true).show()
    }

    override fun ProgresBarStar() {
        ProgresbarStar()
    }

    override fun deletePoint(idpoint: String) {
        pointsViewModel!!.deletePoint(idpoint)
    }
    //endregion View
}
